﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System;
using System.IO;
using System.Web.Script.Serialization;
using System.Xml;
using System.Windows.Forms;
using Newtonsoft.Json.Linq;

namespace Serwis
{
    class Json
    {
        private string path = System.IO.Path.GetDirectoryName(Environment.GetCommandLineArgs()[0]);

        private string returnFile(string pathAndFile)
        {
            string textFile = "";
            try
            {
                using (StreamReader sr = new StreamReader(pathAndFile))
                {
                    // Read the stream to a string, and write the string to the console.
                    textFile = sr.ReadToEnd();
                }
            }
            catch(Exception ex)
            {
                throw new Exception("Błąd podczas kopiowania zawartości pliku! " + ex.Message);
            }
            return textFile;
        }

        public void SaveCnnStr(connectionString cs)
        {

            try
            {
                string file = path + "\\ConnectionString.json";
                string json = new JavaScriptSerializer().Serialize(cs);
                List<connectionString> oldcss = new List<connectionString>();
                if (File.Exists(file))
                {
                    oldcss = ReadListCnnStr();
                }
                oldcss.Add(JsonToCnnStr(json));
                string listcs = new JavaScriptSerializer().Serialize(oldcss);

                System.IO.File.WriteAllText(file, listcs);
            }
            catch (Exception ex)
            {
                MessageBox.Show(String.Format("Błąd podczas zapisu pliku ConnectionString.json. {0}", ex.Message), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        public List<connectionString> ReadListCnnStr()
        {
            List<connectionString> css = new List<connectionString>();
            try
            {
                string file = path + "\\ConnectionString.json";
                if (File.Exists(file))
                {

                    string[] lines = System.IO.File.ReadAllLines(file);
                    foreach (string line in lines)
                    {
                        var serializer = new JavaScriptSerializer();
                        var deserializedResult = serializer.Deserialize<List<connectionString>>(line);
                        css = deserializedResult;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(String.Format("Błąd podczas odczytu pliku ConnectionString.json. {0}", ex.Message), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return css;
        }

        public void DellCnnStr(string nazwa)
        {
            try
            {
                string file = path + "\\ConnectionString.json";
                List<connectionString> css = ReadListCnnStr();
                List<connectionString> newcss = new List<connectionString>();
                File.Delete(file);
                foreach (connectionString cs in css)
                {
                    if (cs.Nazwa != nazwa)
                        SaveCnnStr(cs);
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show("Błąd podczas usuwania pliku! " + ex.Message,"Error",MessageBoxButtons.OK,MessageBoxIcon.Error);
            }

        }

        public connectionString JsonToCnnStr(string json)
        {
            connectionString cs = new connectionString();
            try
            {
                var serializer = new JavaScriptSerializer();
                var deserializedResult = serializer.Deserialize<connectionString>(json);
                cs = deserializedResult;
            }
            catch (Exception ex)
            {
                MessageBox.Show(String.Format("Błąd podczas odczytu pliku ConnectionString.json. {0}", ex.Message), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return cs;
        }

    }

    class connectionString
    {
        public string Nazwa { get; set; }
        public string Serwer { get; set; }
        public string Baza { get; set; }
        public string LoginSzyfr { get; set; }
        public string PassSzyfr { get; set; }

        public string PassSzyfrToString()
        {
            string Pass = szyfr.SzyfrToString(PassSzyfr);
            return Pass;
        }

        public string LoginSzyfrToString()
        {
            string Login = szyfr.SzyfrToString(LoginSzyfr);
            return Login;
        }

        public void PassToSzyfr(string pass)
        {
            PassSzyfr = szyfr.Szyfrowanie(pass, szyfr.Klucz);
        }

        public void LoginToSzyfr(string login)
        {
            LoginSzyfr = szyfr.Szyfrowanie(login, szyfr.Klucz);
        }
    }
}
