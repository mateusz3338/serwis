﻿namespace Serwis
{
    partial class Knt_EditAndCreate
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.textBox_kontakt = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.textBox_numerLokalu = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.textBox_numerDomu = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label_kntAkronim = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.textBox_ulica = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.textBox_kod = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.textBox_miejscowosc = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.textBox_nip = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.textBox_nazwa3 = new System.Windows.Forms.TextBox();
            this.textBox_nazwa2 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textBox_nazwa1 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox_akrnoim = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.button_cancel = new System.Windows.Forms.Button();
            this.button_ok = new System.Windows.Forms.Button();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 90F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.tableLayoutPanel1.Controls.Add(this.panel1, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.panel2, 1, 2);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(12, 12);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 85F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(716, 421);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.Controls.Add(this.textBox_kontakt);
            this.panel1.Controls.Add(this.label12);
            this.panel1.Controls.Add(this.checkBox1);
            this.panel1.Controls.Add(this.textBox_numerLokalu);
            this.panel1.Controls.Add(this.label11);
            this.panel1.Controls.Add(this.textBox_numerDomu);
            this.panel1.Controls.Add(this.label10);
            this.panel1.Controls.Add(this.label_kntAkronim);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.textBox_ulica);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.textBox_kod);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.textBox_miejscowosc);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.textBox_nip);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.textBox_nazwa3);
            this.panel1.Controls.Add(this.textBox_nazwa2);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.textBox_nazwa1);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.textBox_akrnoim);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(38, 24);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(638, 351);
            this.panel1.TabIndex = 0;
            // 
            // textBox_kontakt
            // 
            this.textBox_kontakt.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox_kontakt.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBox_kontakt.ImeMode = System.Windows.Forms.ImeMode.On;
            this.textBox_kontakt.Location = new System.Drawing.Point(325, 209);
            this.textBox_kontakt.MaxLength = 255;
            this.textBox_kontakt.Name = "textBox_kontakt";
            this.textBox_kontakt.Size = new System.Drawing.Size(310, 22);
            this.textBox_kontakt.TabIndex = 21;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label12.Location = new System.Drawing.Point(271, 212);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(48, 15);
            this.label12.TabIndex = 23;
            this.label12.Text = "Kontak:";
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(271, 238);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(96, 17);
            this.checkBox1.TabIndex = 24;
            this.checkBox1.Text = "Czy archiwalny";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // textBox_numerLokalu
            // 
            this.textBox_numerLokalu.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBox_numerLokalu.ImeMode = System.Windows.Forms.ImeMode.On;
            this.textBox_numerLokalu.Location = new System.Drawing.Point(92, 236);
            this.textBox_numerLokalu.MaxLength = 10;
            this.textBox_numerLokalu.Name = "textBox_numerLokalu";
            this.textBox_numerLokalu.Size = new System.Drawing.Size(173, 22);
            this.textBox_numerLokalu.TabIndex = 22;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label11.Location = new System.Drawing.Point(3, 237);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(84, 15);
            this.label11.TabIndex = 20;
            this.label11.Text = "Numer lokalu:";
            // 
            // textBox_numerDomu
            // 
            this.textBox_numerDomu.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBox_numerDomu.ImeMode = System.Windows.Forms.ImeMode.On;
            this.textBox_numerDomu.Location = new System.Drawing.Point(92, 209);
            this.textBox_numerDomu.MaxLength = 10;
            this.textBox_numerDomu.Name = "textBox_numerDomu";
            this.textBox_numerDomu.Size = new System.Drawing.Size(173, 22);
            this.textBox_numerDomu.TabIndex = 19;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label10.Location = new System.Drawing.Point(3, 212);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(83, 15);
            this.label10.TabIndex = 18;
            this.label10.Text = "Numer domu:";
            // 
            // label_kntAkronim
            // 
            this.label_kntAkronim.AutoSize = true;
            this.label_kntAkronim.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label_kntAkronim.Location = new System.Drawing.Point(91, 4);
            this.label_kntAkronim.Name = "label_kntAkronim";
            this.label_kntAkronim.Size = new System.Drawing.Size(71, 16);
            this.label_kntAkronim.TabIndex = 17;
            this.label_kntAkronim.Text = "kntAkronm";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label9.Location = new System.Drawing.Point(6, 4);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(85, 16);
            this.label9.TabIndex = 16;
            this.label9.Text = "Kontrahent:";
            // 
            // textBox_ulica
            // 
            this.textBox_ulica.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox_ulica.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBox_ulica.ImeMode = System.Windows.Forms.ImeMode.On;
            this.textBox_ulica.Location = new System.Drawing.Point(315, 181);
            this.textBox_ulica.MaxLength = 50;
            this.textBox_ulica.Name = "textBox_ulica";
            this.textBox_ulica.Size = new System.Drawing.Size(320, 22);
            this.textBox_ulica.TabIndex = 15;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label8.Location = new System.Drawing.Point(271, 184);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(38, 15);
            this.label8.TabIndex = 14;
            this.label8.Text = "Ulica:";
            // 
            // textBox_kod
            // 
            this.textBox_kod.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBox_kod.ImeMode = System.Windows.Forms.ImeMode.On;
            this.textBox_kod.Location = new System.Drawing.Point(94, 181);
            this.textBox_kod.MaxLength = 6;
            this.textBox_kod.Name = "textBox_kod";
            this.textBox_kod.Size = new System.Drawing.Size(171, 22);
            this.textBox_kod.TabIndex = 13;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label7.Location = new System.Drawing.Point(3, 184);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(85, 15);
            this.label7.TabIndex = 12;
            this.label7.Text = "Kod pocztowy:";
            // 
            // textBox_miejscowosc
            // 
            this.textBox_miejscowosc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox_miejscowosc.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBox_miejscowosc.ImeMode = System.Windows.Forms.ImeMode.On;
            this.textBox_miejscowosc.Location = new System.Drawing.Point(358, 153);
            this.textBox_miejscowosc.MaxLength = 50;
            this.textBox_miejscowosc.Name = "textBox_miejscowosc";
            this.textBox_miejscowosc.Size = new System.Drawing.Size(277, 22);
            this.textBox_miejscowosc.TabIndex = 11;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label6.Location = new System.Drawing.Point(271, 156);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(81, 15);
            this.label6.TabIndex = 10;
            this.label6.Text = "Miejscowosc:";
            // 
            // textBox_nip
            // 
            this.textBox_nip.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBox_nip.ImeMode = System.Windows.Forms.ImeMode.On;
            this.textBox_nip.Location = new System.Drawing.Point(39, 153);
            this.textBox_nip.MaxLength = 15;
            this.textBox_nip.Name = "textBox_nip";
            this.textBox_nip.Size = new System.Drawing.Size(226, 22);
            this.textBox_nip.TabIndex = 9;
            this.textBox_nip.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_nip_KeyPress);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label5.Location = new System.Drawing.Point(3, 156);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(30, 15);
            this.label5.TabIndex = 8;
            this.label5.Text = "NIP:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label4.Location = new System.Drawing.Point(3, 128);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(55, 15);
            this.label4.TabIndex = 7;
            this.label4.Text = "Nazwa3:";
            // 
            // textBox_nazwa3
            // 
            this.textBox_nazwa3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox_nazwa3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBox_nazwa3.ImeMode = System.Windows.Forms.ImeMode.On;
            this.textBox_nazwa3.Location = new System.Drawing.Point(69, 125);
            this.textBox_nazwa3.MaxLength = 50;
            this.textBox_nazwa3.Name = "textBox_nazwa3";
            this.textBox_nazwa3.Size = new System.Drawing.Size(566, 22);
            this.textBox_nazwa3.TabIndex = 6;
            // 
            // textBox_nazwa2
            // 
            this.textBox_nazwa2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox_nazwa2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBox_nazwa2.ImeMode = System.Windows.Forms.ImeMode.On;
            this.textBox_nazwa2.Location = new System.Drawing.Point(69, 97);
            this.textBox_nazwa2.MaxLength = 50;
            this.textBox_nazwa2.Name = "textBox_nazwa2";
            this.textBox_nazwa2.Size = new System.Drawing.Size(566, 22);
            this.textBox_nazwa2.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label3.Location = new System.Drawing.Point(3, 100);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(55, 15);
            this.label3.TabIndex = 4;
            this.label3.Text = "Nazwa2:";
            // 
            // textBox_nazwa1
            // 
            this.textBox_nazwa1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox_nazwa1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBox_nazwa1.ImeMode = System.Windows.Forms.ImeMode.On;
            this.textBox_nazwa1.Location = new System.Drawing.Point(69, 69);
            this.textBox_nazwa1.MaxLength = 50;
            this.textBox_nazwa1.Name = "textBox_nazwa1";
            this.textBox_nazwa1.Size = new System.Drawing.Size(566, 22);
            this.textBox_nazwa1.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label2.Location = new System.Drawing.Point(3, 72);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(62, 15);
            this.label2.TabIndex = 2;
            this.label2.Text = "Nazwa1:";
            // 
            // textBox_akrnoim
            // 
            this.textBox_akrnoim.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox_akrnoim.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBox_akrnoim.ImeMode = System.Windows.Forms.ImeMode.On;
            this.textBox_akrnoim.Location = new System.Drawing.Point(69, 41);
            this.textBox_akrnoim.MaxLength = 50;
            this.textBox_akrnoim.Name = "textBox_akrnoim";
            this.textBox_akrnoim.Size = new System.Drawing.Size(566, 22);
            this.textBox_akrnoim.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(3, 44);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "Akronim:";
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.Controls.Add(this.button_cancel);
            this.panel2.Controls.Add(this.button_ok);
            this.panel2.Location = new System.Drawing.Point(38, 381);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(638, 37);
            this.panel2.TabIndex = 1;
            // 
            // button_cancel
            // 
            this.button_cancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button_cancel.Image = global::Serwis.Properties.Resources.Cancle16x16;
            this.button_cancel.Location = new System.Drawing.Point(459, 8);
            this.button_cancel.Name = "button_cancel";
            this.button_cancel.Size = new System.Drawing.Size(85, 26);
            this.button_cancel.TabIndex = 26;
            this.button_cancel.Text = "Anuluj";
            this.button_cancel.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button_cancel.UseVisualStyleBackColor = true;
            this.button_cancel.Click += new System.EventHandler(this.button_cancel_Click);
            // 
            // button_ok
            // 
            this.button_ok.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button_ok.Image = global::Serwis.Properties.Resources.Add16x16;
            this.button_ok.Location = new System.Drawing.Point(550, 8);
            this.button_ok.Name = "button_ok";
            this.button_ok.Size = new System.Drawing.Size(85, 26);
            this.button_ok.TabIndex = 25;
            this.button_ok.Text = "Zatwierdź";
            this.button_ok.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button_ok.UseVisualStyleBackColor = true;
            this.button_ok.Click += new System.EventHandler(this.button_ok_Click);
            // 
            // Knt_EditAndCreate
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(740, 445);
            this.Controls.Add(this.tableLayoutPanel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Knt_EditAndCreate";
            this.Text = "Knt_EditAndCreate";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Knt_EditAndCreate_FormClosed);
            this.Load += new System.EventHandler(this.Knt_EditAndCreate_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox textBox_kontakt;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.TextBox textBox_numerLokalu;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox textBox_numerDomu;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label_kntAkronim;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox textBox_ulica;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox textBox_kod;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox textBox_miejscowosc;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBox_nip;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBox_nazwa3;
        private System.Windows.Forms.TextBox textBox_nazwa2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBox_nazwa1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBox_akrnoim;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button_cancel;
        private System.Windows.Forms.Button button_ok;
        private System.Windows.Forms.Panel panel2;
    }
}