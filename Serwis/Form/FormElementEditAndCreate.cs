﻿using Dapper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Serwis
{
    public partial class FormElementEditAndCreate : Form
    {
        private FormDokumentEditAndCreate formDocEditAndCreate;
        private Elements elm;
        private bool trybEdit;
        private int index;
        public int id_doc;
        string status;
        public List<SprzetList> sprList;
        private SqlConnection cnn;


        public FormElementEditAndCreate(FormDokumentEditAndCreate formDocEditAndCreate, Elements elm, SqlConnection cnn, bool trybEdit, int index,string status)
        {
            this.index = index;
            this.trybEdit = trybEdit;
            this.formDocEditAndCreate = formDocEditAndCreate;
            this.cnn = cnn;
            this.elm = elm;
            this.id_doc = elm.ID_DOC;
            this.status = status;
            InitializeComponent();
            UstawDane();
            ustawForma();

        }

        public FormElementEditAndCreate(FormDokumentEditAndCreate formDocEditAndCreate, SqlConnection cnn,int id_doc, bool trybEdit, string status)
        {
            this.trybEdit = trybEdit;
            this.formDocEditAndCreate = formDocEditAndCreate;
            this.cnn = cnn;
            this.id_doc = id_doc;
            InitializeComponent();
            this.status = status;
            dateTimePicker_DataPrzejecia.Text = DateTime.Now.ToString();
            label9.Text = "Nowy Element";
            ustawForma();
        }

        private void FormElementEditAndCreate_Load(object sender, EventArgs e)
        {

        }

        private void ustawForma()
        {
            if(!trybEdit)
                label_elmLP.Visible = false;
            textBox_OPEWYK.Enabled = false;
            if (status == "Bufor")
            {
                comboBox_SPR.Enabled = true;
                richTextBox_OpisUszkodzenia.Enabled = true;
                richTextBox_OpiszNaprawy.Enabled = false;
                numericUpDown_wycena.Enabled = true;
                numericUpDown_koszt.Enabled = false;
                dateTimePicker_DataPrzejecia.Enabled = true;
                checkBox_dataZakonczenia.Enabled = false;
                checkBox_dataOdbioru.Enabled = false;
                checkBox_CzyNaprawiono.Enabled = false;
            }
            else if(status == "Zatwierdzone" || status == "W Realizacji")
            {
                comboBox_SPR.Enabled = false;
                richTextBox_OpisUszkodzenia.Enabled = false;
                richTextBox_OpiszNaprawy.Enabled = true;
                numericUpDown_wycena.Enabled = true;
                numericUpDown_koszt.Enabled = true;
                dateTimePicker_DataPrzejecia.Enabled = false;
                checkBox_dataZakonczenia.Enabled = true;
                checkBox_dataOdbioru.Enabled = true;
                checkBox_CzyNaprawiono.Enabled = true;
            }
            else
            {
                dateTimePicker_dataZakonczenia.Enabled = false;
                dateTimePicker_dataOdbioru.Enabled = false;
                comboBox_SPR.Enabled = false;
                richTextBox_OpisUszkodzenia.Enabled = false;
                richTextBox_OpiszNaprawy.Enabled = false;
                numericUpDown_wycena.Enabled = false;
                numericUpDown_koszt.Enabled = false;
                dateTimePicker_DataPrzejecia.Enabled = false;
                checkBox_dataZakonczenia.Enabled = false;
                checkBox_dataOdbioru.Enabled = false;
                checkBox_CzyNaprawiono.Enabled = false;
            }
        }

        private void UstawDane()
        {
            try
            {
                label_elmLP.Text = elm.Lp.ToString();
                textBox_OPEWYK.Text = elm.OperatorW;
                comboBox_SPR.Text = elm.Sprzet;
                richTextBox_OpisUszkodzenia.Text = elm.OpisUszkodzenia;
                richTextBox_OpiszNaprawy.Text = elm.OpisNaprawy;
                numericUpDown_koszt.Value = elm.Koszt;
                numericUpDown_wycena.Value = elm.Wycena;
                if (elm.DataOdbioru != Convert.ToDateTime(null))
                {
                    dateTimePicker_dataOdbioru.Text = elm.DataOdbioru.ToString();
                    checkBox_dataOdbioru.CheckState = CheckState.Checked;
                    dateTimePicker_dataOdbioru.Visible = true;
                }
                if (elm.DataPrzyjecia != Convert.ToDateTime(null))
                {
                    dateTimePicker_DataPrzejecia.Text = elm.DataPrzyjecia.ToString();
                }
                if (elm.DataZakonczenia != Convert.ToDateTime(null))
                {
                    checkBox_dataZakonczenia.CheckState = CheckState.Checked;
                    dateTimePicker_dataZakonczenia.Text = elm.DataZakonczenia.ToString();
                    dateTimePicker_dataZakonczenia.Visible = true;
                }
                if (checkBox_dataOdbioru.CheckState == CheckState.Unchecked)
                    dateTimePicker_dataZakonczenia.Visible = false;
                if (elm.CzyNaprawiono == 1)
                    checkBox_CzyNaprawiono.CheckState = CheckState.Checked;
                if(trybEdit)
                {
                    if(elm.ID_SPR > 0)
                        AddElemToComboBox(elm.ID_SPR);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Błąd podczas ładowania danych! " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
            }
        }

        private void AddElemToComboBox(int elm_id)
        {
            sprList = FindSpr();
            comboBox_SPR.DataSource = sprList;
            comboBox_SPR.DisplayMember = "FullNazwa";
            foreach(SprzetList spr in comboBox_SPR.Items)
            {
                if(spr.ID == elm_id)
                {
                    comboBox_SPR.SelectedItem = spr;
                    break;
                }
            }
        }


        private void checkBox_dataOdbioru_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox_dataOdbioru.CheckState == CheckState.Checked)
               dateTimePicker_dataOdbioru.Visible = true;
            else
                dateTimePicker_dataOdbioru.Visible = false;
        }

        private void button_cancel_Click(object sender, EventArgs e)
        {
            try
            {
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Błąd podczas przełaczenia formów! " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void FormElementEditAndCreate_FormClosed(object sender, FormClosedEventArgs e)
        {
            try
            {
                formDocEditAndCreate.Visible = true;
                formDocEditAndCreate.Left = 0;
                formDocEditAndCreate.Top = 2;
                formDocEditAndCreate.Size = new Size(formDocEditAndCreate.formDocList.formMain.Size.Width - 20, formDocEditAndCreate.formDocList.formMain.Size.Height - formDocEditAndCreate.formDocList.formMain.sizeMenu - 70);
                formDocEditAndCreate.formDocList.formMain.formResize = formDocEditAndCreate;
                //formDocEditAndCreate.formMain.toolStrip_menu.Enabled = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Błąd podczas zamykania okna! " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void UpdateSQL(Elements elm)
        {
            try
            {
                IDbConnection db = cnn;
                if (db.State == ConnectionState.Closed)
                    db.Open();
                string sqlQuery = "update dbo.Elements set ID_DOC = @ID_DOC, ID_SPR = @ID_SPR, ID_OPEWYK = @ID_OPEWYK, OpisUszkodzenia = @OpisUszkodzenia, OpisNaprawy = @OpisNaprawy, Koszt = @Koszt, Wycena = @Wycena, CzyNaprawiono = @CzyNaprawiono, DataOdbioru = ";
                if (elm.DataOdbioru != Convert.ToDateTime(null))
                    sqlQuery = sqlQuery + "@DataOdbioru";
                else
                    sqlQuery = sqlQuery + "null";
                sqlQuery = sqlQuery + ", DataPrzyjecia = ";
                if (elm.DataPrzyjecia != Convert.ToDateTime(null))
                    sqlQuery = sqlQuery + "@DataPrzyjecia";
                else
                    sqlQuery = sqlQuery + "null";
                sqlQuery = sqlQuery + ", DataZakonczenia = ";
                if (elm.DataZakonczenia != Convert.ToDateTime(null))
                    sqlQuery = sqlQuery + "@DataZakonczenia";
                else
                    sqlQuery = sqlQuery + "null";
                sqlQuery = sqlQuery + " where ID = @ID ";
                db.Execute(sqlQuery, elm);
            }
            catch (Exception ex)
            {
                throw new Exception("Błąd podczas update! " + ex.Message);
            }
        }

        public void InsertSQL(Elements elm)
        {
            try
            {
                IDbConnection db = cnn;
                if (db.State == ConnectionState.Closed)
                    db.Open();
                string sqlQuery = "insert into dbo.Elements(ID_DOC,ID_SPR,ID_OPEWYK,OpisUszkodzenia,OpisNaprawy,Koszt,Wycena,CzyNaprawiono,DataPrzyjecia,DataZakonczenia,DataOdbioru) select @ID_DOC, @ID_SPR, @ID_OPEWYK, @OpisUszkodzenia, @OpisNaprawy, @Koszt, @Wycena, @CzyNaprawiono, ";
                if (elm.DataPrzyjecia != Convert.ToDateTime(null))
                    sqlQuery = sqlQuery + "@DataPrzyjecia, ";
                else
                    sqlQuery = sqlQuery + "null,";
                if (elm.DataZakonczenia != Convert.ToDateTime(null))
                    sqlQuery = sqlQuery + "@DataZakonczenia, ";
                else
                    sqlQuery = sqlQuery + "null,";
                
                if (elm.DataOdbioru != Convert.ToDateTime(null))
                    sqlQuery = sqlQuery + "@DataOdbioru ";
                else
                    sqlQuery = sqlQuery + "null";
                db.Execute(sqlQuery, elm);
            }
            catch (Exception ex)
            {
                throw new Exception("Błąd podczas insert! " + ex.Message);
            }
        }

        private void button_ok_Click(object sender, EventArgs e)
        {
            if (comboBox_SPR.Text != "" && richTextBox_OpisUszkodzenia.Text != "")
            {
                if (trybEdit)
                {

                    try
                    {
                        int indexspr = comboBox_SPR.SelectedIndex;
                        int sprid;
                        sprid = sprList[indexspr].ID;
                        elm = new Elements
                        {
                            ID = elm.ID,
                            ID_DOC = id_doc,
                            ID_SPR = sprid,
                            ID_OPEWYK = formDocEditAndCreate.formDocList.formMain.opeid,
                            OpisUszkodzenia = richTextBox_OpisUszkodzenia.Text,
                            OpisNaprawy = richTextBox_OpiszNaprawy.Text,
                            Koszt = numericUpDown_koszt.Value,
                            Wycena = numericUpDown_wycena.Value,
                            DataPrzyjecia = Convert.ToDateTime(dateTimePicker_DataPrzejecia.Text)
                        };
                        if (checkBox_dataOdbioru.CheckState == CheckState.Checked)
                            elm.DataOdbioru = Convert.ToDateTime(dateTimePicker_dataOdbioru.Text);
                        if (checkBox_dataZakonczenia.CheckState == CheckState.Checked)
                            elm.DataZakonczenia = Convert.ToDateTime(dateTimePicker_dataZakonczenia.Text);
                        if (checkBox_CzyNaprawiono.CheckState == CheckState.Checked)
                            elm.CzyNaprawiono = 1;
                        else
                            elm.CzyNaprawiono = 0;
                        UpdateSQL(elm);
                        Elements nelm = FindOneElmID(elm.ID);
                        formDocEditAndCreate.updateListAndView(nelm, index);
                        formDocEditAndCreate.AktualizujDoc(status);
                        this.Close();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Błąd podczas update! " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    try
                    {

                        int index = comboBox_SPR.SelectedIndex;
                        int sprid;
                        sprid = sprList[index].ID;
                        elm = new Elements
                        {
                            ID_DOC = id_doc,
                            ID_SPR = sprid,
                            ID_OPEWYK = formDocEditAndCreate.formDocList.formMain.opeid,
                            OpisUszkodzenia = richTextBox_OpisUszkodzenia.Text,
                            OpisNaprawy = richTextBox_OpiszNaprawy.Text,
                            Koszt = numericUpDown_koszt.Value,
                            Wycena = numericUpDown_wycena.Value,
                            DataPrzyjecia = Convert.ToDateTime(dateTimePicker_DataPrzejecia.Text)
                };
                        if (checkBox_dataOdbioru.CheckState == CheckState.Checked)
                            elm.DataOdbioru = Convert.ToDateTime(dateTimePicker_dataOdbioru.Text);
                        if (checkBox_dataZakonczenia.CheckState == CheckState.Checked)
                            elm.DataZakonczenia = Convert.ToDateTime(dateTimePicker_dataZakonczenia.Text);
                        if (checkBox_CzyNaprawiono.CheckState == CheckState.Checked)
                            elm.CzyNaprawiono = 1;
                        else
                            elm.CzyNaprawiono = 0;

                        InsertSQL(elm);
                        Elements nelm = FindOneElm(id_doc, sprid, formDocEditAndCreate.formDocList.formMain.opeid);
                        formDocEditAndCreate.insertListnAndView(nelm);
                        formDocEditAndCreate.AktualizujDoc(status);
                        this.Close();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Błąd podczas dodawania! " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }

            }
            else
            {
                MessageBox.Show("Nie uzupełniono wszystkich pól!", "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private List<SprzetList> FindSpr()
        {
            string sql = "select id, Nazwa + ' '+isnull(model,'') as FullNazwa from dbo.sprzet";

            try
            {
                IDbConnection db = cnn;
                if (db.State == ConnectionState.Closed)
                    db.Open();
                return db.Query<SprzetList>(sql).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception("Błąd podczas pobierania danych! " + ex.Message);
            }
        }

        public Elements FindOneElm(int ID_DOC, int ID_SPR, int OPEID)
        {
            try
            {
                IDbConnection db = cnn;
                if (db.State == ConnectionState.Closed)
                    db.Open();
                string sql = "select top(1)* from (select elm.ID,ID_DOC,ID_SPR,ID_OPEWYK,ope.Imie+' '+ope.Nazwisko as OperatorW,spr.Nazwa +' '+ spr.Model as Sprzet, elm.OpisUszkodzenia,OpisNaprawy,Koszt,Wycena,DataPrzyjecia,DataZakonczenia,DataOdbioru,CzyNaprawiono, isnull(lp,1) as lp from dbo.Elements as elm join dbo.Operatorzy as ope on ope.id = ID_OPEWYK join dbo.sprzet as spr on spr.Id = ID_SPR left join (select ROW_NUMBER() over (partition by id_doc order by id) as lp,id from dbo.Elements) as lp on lp.ID=elm.id where elm.ID_DOC = @ID_DOC and ID_OPEWYK = @OPEID  ) as t where ID_SPR = @ID_SPR order by lp desc";
                return db.Query<Elements>(sql, new { ID_DOC,ID_SPR,OPEID }).SingleOrDefault();
            }
            catch (Exception ex)
            {
                throw new Exception("Błąd podczas selectcie po insercie! " + ex.Message);
            }
        }

        public Elements FindOneElmID(int ID_ELM)
        {
            try
            {
                IDbConnection db = cnn;
                if (db.State == ConnectionState.Closed)
                    db.Open();
                string sql = "select elm.ID,ID_DOC,ID_SPR,ID_OPEWYK,ope.Imie+' '+ope.Nazwisko as OperatorW,spr.Nazwa +' '+ spr.Model as Sprzet, elm.OpisUszkodzenia,OpisNaprawy,Koszt,Wycena,DataPrzyjecia,DataZakonczenia,DataOdbioru,CzyNaprawiono, isnull(lp,1) as lp from dbo.Elements as elm join dbo.Operatorzy as ope on ope.id = ID_OPEWYK join dbo.sprzet as spr on spr.Id = ID_SPR left join (select ROW_NUMBER() over (partition by id_doc order by id) as lp,id from dbo.Elements) as lp on lp.ID=elm.id where elm.ID = @ID_ELM";
                return db.Query<Elements>(sql, new { ID_ELM }).SingleOrDefault();
            }
            catch (Exception ex)
            {
                throw new Exception("Błąd podczas selectcie po insercie! " + ex.Message);
            }
        }

        private void checkBox_dataZakonczenia_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox_dataZakonczenia.CheckState == CheckState.Checked)
                dateTimePicker_dataZakonczenia.Visible = true;
            else
                dateTimePicker_dataZakonczenia.Visible = false;
        }


        private void comboBox_SPR_DropDown(object sender, EventArgs e)
        {
            sprList = FindSpr();
            comboBox_SPR.DataSource = sprList;
            comboBox_SPR.DisplayMember = "FullNazwa";
        }
    }
}
