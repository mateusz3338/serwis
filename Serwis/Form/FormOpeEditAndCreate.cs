﻿using Dapper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Serwis
{
    public partial class FormOpeEditAndCreate : Form
    {
        private FormOpeList formOpeList;
        private Operatorzy ope;
        private bool trybEdit;
        private int index;
        private SqlConnection cnn;
        public FormOpeEditAndCreate(FormOpeList formOpeList, Operatorzy ope, SqlConnection cnn, bool trybEdit, int index)
        {
            this.index = index;
            this.trybEdit = trybEdit;
            this.formOpeList = formOpeList;
            this.cnn = cnn;
            this.ope = ope;
            InitializeComponent();
            UstawDane();

        }

        public FormOpeEditAndCreate(FormOpeList formOpeList, SqlConnection cnn, bool trybEdit)
        {
            this.trybEdit = trybEdit;
            this.formOpeList = formOpeList;
            this.cnn = cnn;
            InitializeComponent();
            label9.Text = "Nowy Operator";
            label_opeNazwa.Visible = false;
            dateTimePicker1.Visible = false;
            dateTimePicker2.Visible = false;
        }

        private void UstawDane()
        {
            try
            {
                label_opeNazwa.Text = ope.Imie +' ' +ope.Nazwisko;
                textBox_imie.Text = ope.Imie;
                textBox_nazwisko.Text = ope.Nazwisko;
                textBox_login.Text = szyfr.SzyfrToString(ope.Login);
                textBox_haslo.Text = szyfr.SzyfrToString(ope.Haslo);
                if(ope.DataZatrudnienia != Convert.ToDateTime(null))
                {
                    checkBox_dataZatrudnienia.CheckState = CheckState.Checked;
                    dateTimePicker1.Text = ope.DataZatrudnienia.ToString();
                }
                if (ope.DataZwolnienia != Convert.ToDateTime(null))
                {
                    checkBox_dataZwolnienia.CheckState = CheckState.Checked;
                    dateTimePicker2.Text = ope.DataZwolnienia.ToString();
                }
                if (checkBox_dataZatrudnienia.CheckState == CheckState.Unchecked)
                    dateTimePicker1.Visible = false;
                if (checkBox_dataZwolnienia.CheckState == CheckState.Unchecked)
                    dateTimePicker2.Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Błąd podczas ładowania danych! " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
            }
        }

        private void checkBox_dataZatrudnienia_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox_dataZatrudnienia.CheckState == CheckState.Checked)
                dateTimePicker1.Visible = true;
            else
                dateTimePicker1.Visible = false;
        }

        private void checkBox_dataZwolnienia_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox_dataZwolnienia.CheckState == CheckState.Checked)
                dateTimePicker2.Visible = true;
            else
                dateTimePicker2.Visible = false;
        }

        private void button_cancel_Click(object sender, EventArgs e)
        {
            try
            {
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Błąd podczas przełaczenia formów! " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button_ok_Click(object sender, EventArgs e)
        {
            if (textBox_imie.Text != "" && textBox_nazwisko.Text != "" && textBox_haslo.Text != "" && textBox_login.Text !="")
            {
                if (trybEdit)
                {

                    try
                    {
                        ope = new Operatorzy
                        {
                            ID = ope.ID,
                            Imie = textBox_imie.Text,
                            Nazwisko = textBox_nazwisko.Text,
                            Login = szyfr.Szyfrowanie(textBox_login.Text, szyfr.Klucz),
                            Haslo = szyfr.Szyfrowanie(textBox_haslo.Text, szyfr.Klucz),
                        };
                        if (checkBox_dataZatrudnienia.CheckState == CheckState.Checked)
                            ope.DataZatrudnienia = Convert.ToDateTime(dateTimePicker1.Text);
                        if (checkBox_dataZwolnienia.CheckState == CheckState.Checked)
                            ope.DataZwolnienia = Convert.ToDateTime(dateTimePicker2.Text);
                        UpdateSQL(ope);
                        formOpeList.updateListAndView(ope, index);
                        this.Close();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Błąd podczas update! " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    try
                    {
                        ope = new Operatorzy
                        {
                            Imie = textBox_imie.Text,
                            Nazwisko = textBox_nazwisko.Text,
                            Login = szyfr.Szyfrowanie(textBox_login.Text, szyfr.Klucz),
                            Haslo = szyfr.Szyfrowanie(textBox_haslo.Text, szyfr.Klucz),
                        };
                        if (checkBox_dataZatrudnienia.CheckState == CheckState.Checked)
                            ope.DataZatrudnienia = Convert.ToDateTime(dateTimePicker1.Text);
                        else
                            ope.DataZatrudnienia = Convert.ToDateTime(null);
                        if (checkBox_dataZwolnienia.CheckState == CheckState.Checked)
                            ope.DataZwolnienia = Convert.ToDateTime(dateTimePicker2.Text);
                        else
                            ope.DataZwolnienia = Convert.ToDateTime(null);
                        InsertSQL(ope);
                        Operatorzy nope = FindOneOpe(ope.Imie, ope.Nazwisko, ope.Login);
                        formOpeList.insertListnAndView(nope);
                        this.Close();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Błąd podczas dodawania! " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }

            }
            else
            {
                MessageBox.Show("Nie uzupełniono wszystkich pól!", "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void FormOpeEditAndCreate_FormClosed(object sender, FormClosedEventArgs e)
        {
            try
            {
                formOpeList.Visible = true;
                formOpeList.Left = 0;
                formOpeList.Top = 2;
                formOpeList.Size = new Size(formOpeList.formMain.Size.Width - 20, formOpeList.formMain.Size.Height - formOpeList.formMain.sizeMenu - 70);
                formOpeList.formMain.formResize = formOpeList;
                formOpeList.formMain.toolStrip_menu.Enabled = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Błąd podczas zamykania okna! " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public Operatorzy FindOneOpe(string imie, string nazwisko, string login)
        {
            try
            {
                IDbConnection db = cnn;
                if (db.State == ConnectionState.Closed)
                    db.Open();
                string sql = "select top(1) * from dbo.Operatorzy where imie = @imie and nazwisko = @nazwisko and Login = @login";
                return db.Query<Operatorzy>(sql, new { imie, nazwisko, login }).SingleOrDefault();
            }
            catch (Exception ex)
            {
                throw new Exception("Błąd podczas selectcie po insercie! " + ex.Message);
            }
        }

        public void UpdateSQL(Operatorzy ope)
        {
            try
            {
                IDbConnection db = cnn;
                if (db.State == ConnectionState.Closed)
                    db.Open();
                string sqlQuery = "update dbo.Operatorzy set Imie = @Imie, Nazwisko = @Nazwisko, Login = @Login, Haslo = @Haslo, DataZatrudnienia = ";
                if (ope.DataZatrudnienia != Convert.ToDateTime(null))
                    sqlQuery = sqlQuery + "@DataZatrudnienia";
                else
                    sqlQuery = sqlQuery + "null";
                sqlQuery = sqlQuery + ", DataZwolnienia = ";
                if (ope.DataZwolnienia != Convert.ToDateTime(null))
                    sqlQuery = sqlQuery + "@DataZwolnienia";
                else
                    sqlQuery = sqlQuery + "null";
                sqlQuery = sqlQuery + " where id = @id";
                db.Execute(sqlQuery, ope);
            }
            catch (Exception ex)
            {
                throw new Exception("Błąd podczas update! " + ex.Message);
            }
        }

        public void InsertSQL(Operatorzy ope)
        {
            try
            {
                IDbConnection db = cnn;
                if (db.State == ConnectionState.Closed)
                    db.Open();
                string sqlQuery = "insert into dbo.Operatorzy select @Imie, @Nazwisko,";
                if (ope.DataZatrudnienia != Convert.ToDateTime(null))
                    sqlQuery = sqlQuery + "@DataZatrudnienia,";
                else
                    sqlQuery = sqlQuery + "null,";
                if (ope.DataZwolnienia != Convert.ToDateTime(null))
                    sqlQuery = sqlQuery + "@DataZwolnienia";
                else
                    sqlQuery = sqlQuery + "null";
                sqlQuery = sqlQuery + ", @Login, @Haslo";
                db.Execute(sqlQuery, ope);
            }
            catch (Exception ex)
            {
                throw new Exception("Błąd podczas insert! " + ex.Message);
            }
        }

        private void FormOpeEditAndCreate_Load(object sender, EventArgs e)
        {

        }
    }
}
