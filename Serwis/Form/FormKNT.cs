﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Dapper;

namespace Serwis
{
    public partial class FormKNT : Form
    {
        private int sizeMenu;
        public FormMain formMain;
        public bool onFormEdit;
        public SqlConnection cnn;
        private List<Knt> kntlist;
        private Knt_EditAndCreate formkntedit;

        public FormKNT(int sizeMenu, FormMain formMain, SqlConnection cnn)
        {
            this.formMain = formMain;
            this.sizeMenu = sizeMenu;
            this.cnn = cnn;
            onFormEdit = false;
            InitializeComponent();
            try
            {
                kntlist = ReadKntList();
                datagiredview1_Odswierz();
                UstawTabele();
            }
            catch(Exception ex)
            {
                MessageBox.Show("Błąd podczas ładownia tabeli! " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void FormKNT_Load(object sender, EventArgs e)
        {

        }

        public List<Knt> ReadKntList()
        {
            string sql = "select id,Akronim, Nazwa1 as Nazwa,NIP,Miejscowosc from dbo.kontrahenci where CzyArchiwalny = 0";

            IDbConnection db = cnn;
            if (db.State == ConnectionState.Closed)
                db.Open();
            return db.Query<Knt>(sql).ToList();

        }

        private void UstawTabele()
        {
            try
            {
                dataGridView1.Columns["ID"].Visible = false;
                dataGridView1.Columns["Akronim"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                dataGridView1.Columns["Nazwa"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                dataGridView1.Columns["NIP"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                dataGridView1.Columns["Miejscowosc"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                dataGridView1.Columns["NIP"].ValueType = typeof(int);
                ((DataGridViewTextBoxColumn)dataGridView1.Columns["Akronim"]).MaxInputLength = 50;
                ((DataGridViewTextBoxColumn)dataGridView1.Columns["Nazwa"]).MaxInputLength = 50;
                ((DataGridViewTextBoxColumn)dataGridView1.Columns["Miejscowosc"]).MaxInputLength = 50;
                ((DataGridViewTextBoxColumn)dataGridView1.Columns["NIP"]).MaxInputLength = 15;
                dataGridView1.Refresh();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Błąd podczas ustawiania tabeli. " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button_szukaj_Click(object sender, EventArgs e)
        {
            int czyA = (int)checkBox_archiwalny.CheckState;
            string text = textBox1.Text;
            try
            {
                kntlist = Find(text, czyA);
                datagiredview1_Odswierz();
            }
            catch(Exception ex)
            {
                MessageBox.Show("Błąd podczas szukania! " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public List<Knt> Find(string text,int czyArchiwany)
        {
            IDbConnection db = cnn;
            if (db.State == ConnectionState.Closed)
                db.Open();
            string sql = "select id,Akronim, Nazwa1 as Nazwa,NIP,Miejscowosc from dbo.kontrahenci where (nazwa1 like '%'+@text+'%' or Akronim like '%'+@text+'%' or Miejscowosc like '%'+@text+'%' or cast(NIP as varchar(25)) like '%'+@text+'%') and CzyArchiwalny in @czyA";
            return db.Query<Knt>(sql,new { text, czyA = new int[] { 0, czyArchiwany } }).ToList();
        }

        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyData == Keys.Enter)
            {
                int czyA = (int)checkBox_archiwalny.CheckState;
                string text = textBox1.Text;
                try
                {
                    dataGridView1.DataSource = Find(text, czyA);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Błąd podczas szukania! " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void Delete(List<int> id, List<int> indexs)
        {
            IDbConnection db = cnn;
            if (db.State == ConnectionState.Closed)
                db.Open();
            db.Execute("delete from dbo.kontrahenci where ID in @id", new {  id });
            indexs.Sort();
            indexs.Reverse();
            foreach (int index in indexs)
            {
                kntlist.RemoveAt(index);
            }
            dataGridView1.DataSource = kntlist.ToList();
        }

        public void Update(Knt knt)
        {
            IDbConnection db = cnn;
            if (db.State == ConnectionState.Closed)
                db.Open();
            string sqlQuery = "UPDATE dbo.kontrahenci SET Akronim = @Akronim, Nazwa1 = @Nazwa, NIP = @NIP,Miejscowosc = @Miejscowosc WHERE Id = @Id";
            db.Execute(sqlQuery, knt);
        }

        private void dataGridView1_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                Knt knt = kntlist[e.RowIndex];
                Update(knt);
            }
            catch(Exception ex)
            {
                MessageBox.Show("Błąd podczas aktualizowania danych! " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dataGridView1_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            if (e.ColumnIndex == 3) 
            {
                long i;
                if (e.FormattedValue.ToString().Length != 0)
                {
                    if (!long.TryParse(Convert.ToString(e.FormattedValue), out i))
                    {
                        e.Cancel = true;
                        MessageBox.Show("Błąd konwersji na liczbę.\nLiczba jest zbyt duża lub jest pusta!", "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {

                    }
                }
                else
                {

                }
            }
        }

        private void dataGridView1_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            e.Control.KeyPress -= new KeyPressEventHandler(Column1_KeyPress);
            if (dataGridView1.CurrentCell.ColumnIndex == 3) //Desired Column
            {
                TextBox tb = e.Control as TextBox;
                if (tb != null)
                {
                    tb.KeyPress += new KeyPressEventHandler(Column1_KeyPress);
                }
            }
        }

        private void Column1_KeyPress(object sender, KeyPressEventArgs e)
        {
            // allowed only numeric value  ex.10
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
                SystemSounds.Hand.Play();
            }
            else
            {
                
            }

        }

        private void button_delete_Click(object sender, EventArgs e)
        {
            try
            {
                List<int> ids = new List<int>();
                List<int> indexs = new List<int>();
                int oldindex = -1;
                foreach (DataGridViewCell c in dataGridView1.SelectedCells)
                {
                    if (c.RowIndex != oldindex)
                    {
                        DataGridViewRow row = dataGridView1.Rows[c.RowIndex];
                        string akronim = row.Cells["Akronim"].Value.ToString();
                        DialogResult pytanie = MessageBox.Show(String.Format("Czy chcesz usunąć kontrahenta: {0}?", akronim), "Pytanie", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                        if (pytanie == DialogResult.Yes)
                        {
                            indexs.Add(c.RowIndex);
                            ids.Add((int)row.Cells["ID"].Value);
                        }
                    }
                    oldindex = c.RowIndex;
                }
                if (indexs.Count > 0)
                    Delete(ids, indexs);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Błąd podczas usuwania! " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        private void openKntEdit(Knt_AllDane knt,bool trybEdit, int index)
        {
            
            try
            {
                formkntedit = new Knt_EditAndCreate(this, knt, cnn, trybEdit,index);
                formkntedit.StartPosition = FormStartPosition.Manual;
                formkntedit.Left = 0;
                formkntedit.Top = 2;
                formkntedit.Size = new Size(formMain.Size.Width - 20, formMain.Size.Height - sizeMenu - 70);
                formkntedit.MdiParent = formMain;
                this.Visible = false;
                formMain.toolStrip_menu.Enabled = false;
                formMain.formResize = formkntedit;
                onFormEdit = true;
                formkntedit.Show();
            }
            catch(Exception ex)
            {
                try
                {
                    this.Visible = true;
                    formMain.toolStrip_menu.Enabled = true;
                    formMain.formResize = this;
                    onFormEdit = false;
                    formkntedit.Close();
                }
                catch { }
                MessageBox.Show("Błąd podczas uruchamiania okna! " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        
        public void updateListAndView(Knt nknt,int index,int czyA)
        {
            try
            {
                if ((int)checkBox_archiwalny.CheckState == 0 && czyA == 1)
                    kntlist.RemoveAt(index);
                else
                    kntlist[index] = nknt;
                datagiredview1_Odswierz();
            }
            catch { }
        }

        public void insertListnAndView(Knt knt, int czyA)
        {
            if (((int)checkBox_archiwalny.CheckState == 1 && czyA == 1) || ((int)checkBox_archiwalny.CheckState == 0 && czyA == 0))
            {
                if (dataGridView1.Rows.Count == 0)
                {
                    List<Knt> newList = new List<Knt>();
                    foreach (Knt k in kntlist)
                    {
                        newList.Add(k);
                    }
                    newList.Add(knt);
                    kntlist = newList;
                    datagiredview1_Odswierz();
                }
                else
                {
                    kntlist.Add(knt);
                    datagiredview1_Odswierz();
                }

            }
        }

        private void datagiredview1_Odswierz()
        {
            dataGridView1.DataSource = kntlist;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            formMain.onFormKnt = false;
            this.Close();
        }

        public Knt_AllDane FindOneKnt(int id)
        {
            IDbConnection db = cnn;
            if (db.State == ConnectionState.Closed)
                db.Open();
            string sql = "select * from dbo.kontrahenci where id = @id";
            return db.Query<Knt_AllDane>(sql, new { id }).SingleOrDefault();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedCells.Count == 1)
            {
                try
                {
                    int index = dataGridView1.SelectedCells[0].RowIndex;
                    DataGridViewRow row = dataGridView1.Rows[index];
                    int id = (int)row.Cells["ID"].Value;
                    Knt_AllDane knt = FindOneKnt(id);
                    openKntEdit(knt, true, index);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Bład podczas wyświetlania forma! " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

            }
            else
            {
                MessageBox.Show("Wybrano więcej niż jednego kontrahenta!", "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void openKntCreate(bool trybEdit)
        {

            try
            {
                formkntedit = new Knt_EditAndCreate(this, cnn, trybEdit);
                formkntedit.StartPosition = FormStartPosition.Manual;
                formkntedit.Left = 0;
                formkntedit.Top = 2;
                formkntedit.Size = new Size(formMain.Size.Width - 20, formMain.Size.Height - sizeMenu - 70);
                formkntedit.MdiParent = formMain;
                this.Visible = false;
                formMain.toolStrip_menu.Enabled = false;
                formMain.formResize = formkntedit;
                onFormEdit = true;
                formkntedit.Show();
            }
            catch (Exception ex)
            {
                try
                {
                    this.Visible = true;
                    formMain.toolStrip_menu.Enabled = true;
                    formMain.formResize = this;
                    onFormEdit = false;
                    formkntedit.Close();
                }
                catch { }
                MessageBox.Show("Błąd podczas uruchamiania okna! " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            try
            {
                openKntCreate(false);
            }
            catch(Exception ex)
            {
                MessageBox.Show("Błąd podczas uruchamiania forma " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
