﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Serwis
{
    public partial class FormDB_Create : Form
    {
        private FormDBConnection oldForm;
        private bool create;

        public FormDB_Create()
        {
            InitializeComponent();
            this.create = false;
        }


        public FormDB_Create(FormDBConnection oldForm)
        {
            InitializeComponent();
            this.create = false;
            this.oldForm = oldForm;
        }

        public FormDB_Create(FormDBConnection oldForm, bool create)
        {
            InitializeComponent();
            this.create = create;
            this.oldForm = oldForm;
            comboBox_baza.DropDownStyle = ComboBoxStyle.Simple;
            label7.Visible = true;
            label8.Visible = true;
            textBox_User.Visible = true;
            textBox_PassUser.Visible = true;
        }

        private void FormDB_Create_Load(object sender, EventArgs e)
        {

        }

        private void FormDB_Create_FormClosed(object sender, FormClosedEventArgs e)
        {
            oldForm.LoadDane();
            oldForm.StartPosition = FormStartPosition.Manual;
            oldForm.Left = this.Left;
            oldForm.Top = this.Top;
            oldForm.Size = new Size(this.Width, this.Height);
            oldForm.Show();
        }

        private void comboBox_baza_DropDown(object sender, EventArgs e)
        {
            if (create == false)
            {
                try
                {
                    string connetionString = SQL.ConnectionString(textBox_serwer.Text, textBox_login.Text, textBox_pass.Text, "", false);
                    SQL.SettingConnection(connetionString);
                    SQL.cnn.Open();
                    SqlDataAdapter sa = new SqlDataAdapter(Properties.Resources.ListaBaz, SQL.cnn);
                    DataTable dt = new DataTable();
                    sa.Fill(dt);
                    if (dt.Rows.Count > 0)
                    {
                        comboBox_baza.Items.Clear();
                        foreach (DataRow row in dt.Rows)
                        {
                            comboBox_baza.Items.Add(row[0].ToString());
                        }
                    }
                    SQL.cnn.Close();
                }
                catch { }
            }
        }

        private void button_test_Click(object sender, EventArgs e)
        {
            try
            {
                string baza;
                if(create)
                    baza = "";
                else
                    baza = comboBox_baza.Text;

                string connetionString = SQL.ConnectionString(textBox_serwer.Text, textBox_login.Text, textBox_pass.Text, baza, false);
                SQL.SettingConnection(connetionString);
                SQL.cnn.Open();
                MessageBox.Show("Połączono", "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
                SQL.cnn.Close();
            }
            catch(Exception ex)
            {
                MessageBox.Show("Błąd podczas połączenia: " + ex.Message,"Info",MessageBoxButtons.OK,MessageBoxIcon.Information);
            }
        }

        private void button_add_Click(object sender, EventArgs e)
        {
            if (create == false)
                AddToTable();
            else
            {
                bool stworzono = CreateTable();
                if (stworzono)
                    AddToTable();
            }
        }

        private void AddToTable()
        {
            string serwer = textBox_serwer.Text;
            string nazwa = textBox_NazwaP.Text;
            string login = textBox_login.Text;
            string pass = textBox_pass.Text;
            string baza = comboBox_baza.Text;
            if (serwer != "" && nazwa != "" && login != "" && pass != "" && baza != "")
            {
                try
                {
                    if (oldForm.dataT.Rows.Count > 0)
                    {
                        bool exists = false;

                        Json js = new Json();
                        List<connectionString> css = js.ReadListCnnStr(); 
                        foreach(connectionString cs in css)
                        {
                            if(cs.Nazwa == nazwa)
                            {
                                exists = true;
                                break;
                            }
                        }

                        if (exists == false)
                        {
                            save(nazwa,serwer,baza,login,pass);
                            this.Close();
                        }
                        else
                        {
                            MessageBox.Show("Połączenie o takiej nazwie już istnieje!", "Informacja", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    else
                    {
                        save(nazwa, serwer, baza, login, pass);
                        this.Close();
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Bład podczas dodawania rekordu: " + ex.Message,"Error",MessageBoxButtons.OK,MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show("Nieuzupełnione dane!","Info",MessageBoxButtons.OK,MessageBoxIcon.Information);
            }
        }

        private bool CreateTable()
        {
            bool stworzono = false;
            string dataBaseName = comboBox_baza.Text;
            string loginUser = textBox_User.Text.ToLower();
            string passUser = textBox_PassUser.Text;
            string nazwa = textBox_NazwaP.Text;
            string loginsql = textBox_login.Text;
            string passsql = textBox_pass.Text;

            if (dataBaseName != "" && nazwa != "" && loginUser != "" && passUser != "" && loginsql != "" && passsql != "")
            {
                try
                {
                    string connetionString = SQL.ConnectionString(textBox_serwer.Text, textBox_login.Text, textBox_pass.Text, "", false);
                    SQL.SettingConnection(connetionString);
                    SQL.cnn.Open();
                    int czyIstnieje = 0;
                    if (czyIstnieje == 0)
                    {
                        SqlCommand command = new SqlCommand();
                        command.Connection = SQL.cnn;
                        command.CommandText = Properties.Resources.CreateDatabaseSerwis;
                        command.Parameters.Add("@DB", SqlDbType.VarChar).Value = dataBaseName;
                        command.Parameters.Add("@login", SqlDbType.VarChar).Value = szyfr.Szyfrowanie(loginUser, szyfr.Klucz);
                        command.Parameters.Add("@pass", SqlDbType.VarChar).Value = szyfr.Szyfrowanie(passUser, szyfr.Klucz);
                        command.ExecuteNonQuery();
                        SqlConnection cnn2 = new SqlConnection(SQL.ConnectionString(textBox_serwer.Text, textBox_login.Text, textBox_pass.Text, comboBox_baza.Text, false)); 
                        try
                        {
                            cnn2.Open();
                            command.Connection = cnn2;
                            command.CommandText = Properties.Resources._1CreateTable_Kontrahenci;
                            command.ExecuteNonQuery();
                            command.CommandText = Properties.Resources._2CreateTable_sprzet;
                            command.ExecuteNonQuery();
                            command.CommandText = Properties.Resources._3CreateTable_Operatorzy;
                            command.ExecuteNonQuery();
                            command.CommandText = Properties.Resources._4CreateTable_Documents;
                            command.ExecuteNonQuery();
                            command.CommandText = Properties.Resources._5CreateTable_Elements;
                            command.ExecuteNonQuery();
                            command.CommandText = Properties.Resources._6CreateTrigger_Zakonczenie;
                            command.ExecuteNonQuery();
                            command.CommandText = Properties.Resources._7CreateTrigger_UsunElementy;
                            command.ExecuteNonQuery();
                            command.CommandText = Properties.Resources._8CreateTrigger_BlokadaOperatora;
                            command.ExecuteNonQuery();
                            command.CommandText = Properties.Resources._9CreateTrigger_Numerator;
                            command.ExecuteNonQuery();
                            command.CommandText = Properties.Resources._10InsertUser;
                            command.ExecuteNonQuery();
                            cnn2.Close();
                            stworzono = true;
                            MessageBox.Show(String.Format("Baza {0} stworzona!", dataBaseName), "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        catch (Exception ex)
                        {
                            try
                            {
                                try
                                {
                                    cnn2.Close();
                                }
                                catch { }
                                command.Connection = SQL.cnn;
                                command.CommandText = Properties.Resources.DropDatabase;
                                command.ExecuteNonQuery();
                            }
                            catch(Exception exx)
                            {
                                MessageBox.Show(String.Format("Błąd podczas usuwania bazy {0}. Błąd: {1}", dataBaseName, exx.Message));
                            }
                            throw new Exception("Błąd podczas tworzenia bazy! " + ex.Message);
                        }
                    }

                    SQL.cnn.Close();
                }
                catch(Exception ex)
                {
                    MessageBox.Show("Błąd podczas tworzenia bazy! " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show("Nieuzupełnione dane!", "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

           return stworzono;
        }

        private void comboBox_baza_KeyDown(object sender, KeyEventArgs e)
        {
            if (create == false)
            {
                if (e.KeyData == Keys.Enter)
                {
                    if (create == false)
                        AddToTable();
                    else
                    {
                        bool stworzono = CreateTable();
                        if (stworzono)
                            AddToTable();
                    }
                }
            }
        }

        private void save(string nazwa, string serwer, string baza, string login, string pass)
        {
            connectionString cs = new connectionString()
            {
                Nazwa = nazwa,
                Serwer = serwer,
                Baza = baza,
            };
            cs.PassToSzyfr(pass);
            cs.LoginToSzyfr(login);

            Json js = new Json();
            js.SaveCnnStr(cs);
        }

        private void textBox_PassUser_KeyDown(object sender, KeyEventArgs e)
        {
            if (create)
            {
                if (e.KeyData == Keys.Enter)
                {
                    if (create == false)
                        AddToTable();
                    else
                    {
                        bool stworzono = CreateTable();
                        if (stworzono)
                            AddToTable();
                    }
                }
            }
        }
    }
}
