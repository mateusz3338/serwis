﻿using Dapper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Serwis
{
    public partial class FormDokumentEditAndCreate : Form
    {
        private bool trybEdit;
        public FormDocList formDocList;
        private List<KntAkronim> kntlist;
        private Dokument doc;
        private string statusDokumentu;
        private int index;
        private List<string> statuslist;
        private SqlConnection cnn;
        private FormElementEditAndCreate formElemEditAndCreate;
        private List<Elements> elemList;

        public FormDokumentEditAndCreate(FormDocList formDocList, Dokument doc, SqlConnection cnn, bool trybEdit, int index)
        {
            this.trybEdit = trybEdit;
            this.index = index;
            this.formDocList = formDocList;
            this.cnn = cnn;
            this.doc = doc;
            InitializeComponent();
            dataGridView1.ReadOnly = true;
            UstawDane();
        }

        public FormDokumentEditAndCreate(FormDocList formDocList, SqlConnection cnn, bool trybEdit)
        {
            this.trybEdit = trybEdit;
            this.formDocList = formDocList;
            this.cnn = cnn;
            InitializeComponent();
            dataGridView1.ReadOnly = true;
            label9.Text = "Nowy Dokument";
            label_dokNumer.Visible = false;
            UstawDane();
        }

        private void FormDokumentEditAndCreate_Load(object sender, EventArgs e)
        {

        }

        private void UstawDane()
        {
            try
            {
                kntlist = new List<KntAkronim>();
                statuslist = new List<string>();
                if (trybEdit)
                {
                    elemList = ReadElmList(doc.ID);
                    datagiredview1_Odswierz();
                    label_dokNumer.Text = doc.Numer;
                    textBox_operator.Text = doc.Operator;
                    AddToComboboxStatus();
                    if(doc.Status == "Zakończone" || doc.Status == "W Realizacji")
                    {
                        statuslist.Add(doc.Status);
                        comboBox_status.DataSource = null;
                        comboBox_status.DataSource = statuslist;
                    }
                    UstawKNT();
                    comboBox_knt.Text = doc.Kontrahent;
                    dateTimePicker1.Text = doc.DataZgloszenia.ToString();
                    comboBox_status.Text = doc.Status;

                }
                else
                {
                    doc = new Dokument();
                    AddToComboboxStatus();
                    doc.Status = "Bufor";
                    comboBox_status.Text = "Bufor";
                    Operatorzy ope = FindOneOpe();
                    textBox_operator.Text = String.Format("{0} {1}", ope.Imie, ope.Nazwisko);
                    DateTime data = DateTime.Now;
                    dateTimePicker1.Text = data.ToString();
                    elemList = ReadElmList(-1);
                    datagiredview1_Odswierz();
                }
                if(doc.Status == "Zakończone" || doc.Status == "Anulowane")
                {
                    dateTimePicker1.Enabled = false;
                    button_delete.Enabled = false;
                    button4.Enabled = false;
                    comboBox_knt.Enabled = false;
                }
                else
                {
                    dateTimePicker1.Enabled = true;
                    button_delete.Enabled = true;
                    button4.Enabled = true;
                    comboBox_knt.Enabled = true;
                }
                ustawDataGridView();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Błąd podczas ładowania danych! " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
            }
        }


        private void ustawDataGridView()
        {
            dataGridView1.Columns["ID"].Visible = false;
            dataGridView1.Columns["ID_DOC"].Visible = false;
            dataGridView1.Columns["ID_SPR"].Visible = false;
            dataGridView1.Columns["ID_OPEWYK"].Visible = false;
            dataGridView1.Columns["OpisUszkodzenia"].Visible = false;
            dataGridView1.Columns["OpisNaprawy"].Visible = false;

            dataGridView1.Columns["Lp"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            dataGridView1.Columns["OperatorW"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            dataGridView1.Columns["Sprzet"].MinimumWidth = 175;
            dataGridView1.Columns["Sprzet"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            dataGridView1.Columns["Koszt"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            dataGridView1.Columns["Wycena"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            dataGridView1.Columns["DataPrzyjecia"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            dataGridView1.Columns["DataZakonczenia"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            dataGridView1.Columns["DataOdbioru"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            dataGridView1.Columns["CzyNaprawiono"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;

            dataGridView1.Columns["OperatorW"].HeaderText = "Wykonujący";
            dataGridView1.Columns["DataPrzyjecia"].HeaderText = "Data Przyjecia";
            dataGridView1.Columns["DataZakonczenia"].HeaderText = "Data Zakonczenia";
            dataGridView1.Columns["DataOdbioru"].HeaderText = "Data Odbioru";
            dataGridView1.Columns["CzyNaprawiono"].HeaderText = "Czy Naprawiono";

            dataGridView1.Refresh();
        }

        public List<Elements> ReadElmList(int ID_DOC)
        {
            string sql = "select elm.ID,ID_DOC,ID_SPR,ID_OPEWYK,ope.Imie+' '+ope.Nazwisko as OperatorW,spr.Nazwa +' '+ spr.Model as Sprzet, elm.OpisUszkodzenia,OpisNaprawy,Koszt,Wycena,DataPrzyjecia,DataZakonczenia,DataOdbioru,CzyNaprawiono, isnull(lp,1) as lp from dbo.Elements as elm join dbo.Operatorzy as ope on ope.id = ID_OPEWYK join dbo.sprzet as spr on spr.Id = ID_SPR left join (select ROW_NUMBER() over (partition by id_doc order by id) as lp,id from dbo.Elements) as lp on lp.ID=elm.id  where elm.ID_DOC = @ID_DOC";

            try
            {
                IDbConnection db = cnn;
                if (db.State == ConnectionState.Closed)
                    db.Open();
                return db.Query<Elements>(sql, new { ID_DOC }).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception("Błąd podczas selectcie po insercie! " + ex.Message);
            }
        }

        public Operatorzy FindOneOpe()
        {
            try
            {
                int id = formDocList.formMain.opeid;
                IDbConnection db = cnn;
                if (db.State == ConnectionState.Closed)
                    db.Open();
                string sql = "select top(1) * from dbo.Operatorzy where id = @id";
                return db.Query<Operatorzy>(sql, new { id }).SingleOrDefault();
            }
            catch (Exception ex)
            {
                throw new Exception("Błąd podczas szukania Operatora! " + ex.Message);
            }
        }

        public Dokument FindOneDoc(int ID_KNT, int ID_OPE)
        {
            try
            {
                int id = formDocList.formMain.opeid;
                IDbConnection db = cnn;
                if (db.State == ConnectionState.Closed)
                    db.Open();
                string sql = "select top(1) d.id,k.id as ID_KNT, o.id as ID_OPE, k.Akronim as Kontrahent,o.Nazwisko + ' ' + o.Imie as Operator, DataZgloszenia, Numer, case when Status = 1 then 'Bufor' when Status = 2 then 'Zatwierdzone' when Status = 3 then 'W Realizacji' when Status = 4 then 'Zakończone' when Status = 5 then 'Anulowane' end as Status from dbo.Documents as d join dbo.kontrahenci as k on k.ID = d.ID_KNT join dbo.Operatorzy as o on o.id = d.ID_Ope where k.id = @ID_KNT and o.id = @ID_OPE order by d.id desc";
                return db.Query<Dokument>(sql, new { ID_KNT,ID_OPE }).SingleOrDefault();
            }
            catch (Exception ex)
            {
                throw new Exception("Błąd podczas szukania Operatora! " + ex.Message);
            }
        }

        public Dokument FindOneDocID(int ID)
        {
            try
            {
                int id = formDocList.formMain.opeid;
                IDbConnection db = cnn;
                if (db.State == ConnectionState.Closed)
                    db.Open();
                string sql = "select top(1) d.id,k.id as ID_KNT, o.id as ID_OPE, k.Akronim as Kontrahent,o.Nazwisko + ' ' + o.Imie as Operator, DataZgloszenia, Numer, case when Status = 1 then 'Bufor' when Status = 2 then 'Zatwierdzone' when Status = 3 then 'W Realizacji' when Status = 4 then 'Zakończone' when Status = 5 then 'Anulowane' end as Status from dbo.Documents as d join dbo.kontrahenci as k on k.ID = d.ID_KNT join dbo.Operatorzy as o on o.id = d.ID_Ope where d.id = @ID order by d.id desc";
                return db.Query<Dokument>(sql, new { ID }).SingleOrDefault();
            }
            catch (Exception ex)
            {
                throw new Exception("Błąd podczas szukania Operatora! " + ex.Message);
            }
        }

        private void button_cancel_Click(object sender, EventArgs e)
        {
            try
            {
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Błąd podczas przełaczenia formów! " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void FormDokumentEditAndCreate_FormClosed(object sender, FormClosedEventArgs e)
        {
            try
            {
                formDocList.Visible = true;
                formDocList.Left = 0;
                formDocList.Top = 2;
                formDocList.Size = new Size(formDocList.formMain.Size.Width - 20, formDocList.formMain.Size.Height - formDocList.formMain.sizeMenu - 70);
                formDocList.formMain.formResize = formDocList;
                formDocList.formMain.toolStrip_menu.Enabled = true;
                formDocList.datagiredview1_Odswierz();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Błąd podczas zamykania okna! " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void UpdateSQL(Dokument doc)
        {
            try
            {
                IDbConnection db = cnn;
                if (db.State == ConnectionState.Closed)
                    db.Open();
                string sqlQuery = "update dbo.Documents set ID_KNT = @ID_KNT,DataZgloszenia = @DataZgloszenia, Status = case when @Status = 'Bufor' then 1 when @Status = 'Zatwierdzone' then 2 when @Status = 'W Realizacji' then 3 when @Status = 'Zakończone' then 4 else 5 end where id = @ID";
                db.Execute(sqlQuery, doc);
            }
            catch (Exception ex)
            {
                throw new Exception("Błąd podczas update! " + ex.Message);
            }
        }

        public void InsertSQL(Dokument dok)
        {
            try
            {
                IDbConnection db = cnn;
                if (db.State == ConnectionState.Closed)
                    db.Open();
                string sqlQuery = "insert into dbo.Documents(ID_KNT,ID_Ope,DataZgloszenia,Status) select @ID_KNT,@ID_OPE,@DataZgloszenia,case when @Status = 'Bufor' then 1 when @Status = 'Zatwierdzone' then 2 when @Status = 'W Realizacji' then 3 when @Status = 'Zakończone' then 4 else 5 end";
                db.Execute(sqlQuery, doc);
            }
            catch (Exception ex)
            {
                throw new Exception("Błąd podczas insert! " + ex.Message);
            }
        }

        private void comboBox_status_DropDown(object sender, EventArgs e)
        {
            AddToComboboxStatus();
        }

        private void AddToComboboxStatus()
        {
            if (trybEdit)
            {
                statuslist.Clear();
                statuslist.Add("Bufor");
                if (dataGridView1.Rows.Count > 0)
                {
                    statuslist.Add("Zatwierdzone");
                }
                statuslist.Add("Anulowane");
            }
            else
            {
                statuslist.Clear();
                statuslist.Add("Bufor");
                if (dataGridView1.Rows.Count > 0)
                {
                    statuslist.Add("Zatwierdzone");
                }
            }
            comboBox_status.DataSource = null;
            comboBox_status.DataSource = statuslist;
            comboBox_status.Refresh();

        }

        private void comboBox_knt_DropDown(object sender, EventArgs e)
        {
            UstawKNT();
        }

        private void UstawKNT()
        {
            kntlist = FindKnt();
            comboBox_knt.DataSource = kntlist;
            comboBox_knt.DisplayMember = "Akronim";
        }

        private List<KntAkronim> FindKnt()
        {
            string sql = "select id,Akronim from dbo.kontrahenci where CzyArchiwalny = 0";

            try
            {
                IDbConnection db = cnn;
                if (db.State == ConnectionState.Closed)
                    db.Open();
                return db.Query<KntAkronim>(sql).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception("Błąd podczas pobierania danych! " + ex.Message);
            }
        }

        private void button_delete_Click(object sender, EventArgs e)
        {
            try
            {
                List<int> ids = new List<int>();
                List<int> indexs = new List<int>();
                int oldindex = -1;
                foreach (DataGridViewCell c in dataGridView1.SelectedCells)
                {
                    if (c.RowIndex != oldindex)
                    {
                        DataGridViewRow row = dataGridView1.Rows[c.RowIndex];
                        string lp = row.Cells["Lp"].Value.ToString();
                        DialogResult pytanie = MessageBox.Show(String.Format("Czy chcesz usunąć element: {0}?", lp), "Pytanie", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                        if (pytanie == DialogResult.Yes)
                        {
                            indexs.Add(c.RowIndex);
                            ids.Add((int)row.Cells["ID"].Value);
                        }
                    }
                    oldindex = c.RowIndex;
                }
                if (indexs.Count > 0)
                    Delete(ids, indexs);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Błąd podczas usuwania! " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void Delete(List<int> id, List<int> indexs)
        {
            try
            {
                IDbConnection db = cnn;
                if (db.State == ConnectionState.Closed)
                    db.Open();
                db.Execute("delete from dbo.elements where id in @id", new { id });
                indexs.Sort();
                indexs.Reverse();
                foreach (int index in indexs)
                {
                    elemList.RemoveAt(index);
                }
                dataGridView1.DataSource = elemList.ToList();
                dataGridView1.Refresh();
            }
            catch (Exception ex)
            {
                throw new Exception("Błąd podczas usuwania! " + ex.Message);
            }
        }

        private void button_ok_Click(object sender, EventArgs e)
        {
            bool czyZapisano =save();
            if (czyZapisano == true)
            {
                this.Close();
            }
        }

        private bool save()
        {
            if (textBox_operator.Text != "" && comboBox_knt.Text != "" && comboBox_status.Text != "" && dateTimePicker1.Text != "")
            {
                if (trybEdit)
                {

                    try
                    {
                        int indexknt = comboBox_knt.SelectedIndex;
                        int kntid;
                        kntid = kntlist[indexknt].ID;
                        int docid = doc.ID;
                        doc = new Dokument
                        {
                            ID = docid,
                            ID_KNT = kntid,
                            ID_OPE = formDocList.formMain.opeid,
                            Operator = textBox_operator.Text,
                            Kontrahent = comboBox_knt.Text,
                            DataZgloszenia = Convert.ToDateTime(dateTimePicker1.Text),
                            Status = comboBox_status.Text,
                            Numer = doc.Numer
                        };
                        UpdateSQL(doc);
                        formDocList.updateListAndView(doc, index);
                        return true;
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Błąd podczas update! " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return false;
                    }
                }
                else
                {
                    try
                    {
                        int indexknt = comboBox_knt.SelectedIndex;
                        int kntid;
                        kntid = kntlist[indexknt].ID;
                        doc = new Dokument
                        {
                            ID_KNT = kntid,
                            ID_OPE = formDocList.formMain.opeid,
                            Operator = textBox_operator.Text,
                            Kontrahent = comboBox_knt.Text,
                            DataZgloszenia = Convert.ToDateTime(dateTimePicker1.Text),
                            Status = comboBox_status.Text
                        };
                        InsertSQL(doc);
                        Dokument ndoc = FindOneDoc(doc.ID_KNT, doc.ID_OPE);
                        formDocList.insertListnAndView(ndoc);
                        formDocList.AktualizujDoc();
                        
                        return true;
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Błąd podczas dodawania! " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return false;
                    }
                }

            }
            else
            {
                MessageBox.Show("Nie uzupełniono wszystkich pól!", "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }
        }

        public void AktualizujDoc(string statuss)
        {
            doc = FindOneDocID(doc.ID);
            if (doc.Status == "Bufor" || doc.Status == "Zatwierdzone")
                doc.Status = statuss;
            UstawDane();
            datagiredview1_Odswierz();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            int indexstatus = comboBox_status.SelectedIndex;
            string statusComboBox = statuslist[indexstatus];
            if (statusComboBox == "Bufor")
                openOpeCreate(trybEdit);
            else
            {
                MessageBox.Show("Zmień status na bufor!", "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void openOpeCreate(bool trybEdit)
        {
            if (trybEdit)
            {
                Openform(false);

            }
            else
            {
                bool czyZapisano = save();
                if (czyZapisano == true)
                {
                    this.trybEdit = true;
                    doc = FindOneDoc(doc.ID_KNT, formDocList.formMain.opeid);
                    index = formDocList.insertListnAndView(doc);
                    formDocList.AktualizujDoc();
                    Openform(trybEdit);
                }
                
            }
        }
        private void Openform(bool trybEdit)
        {
            try
            {
                int indexstatus = comboBox_status.SelectedIndex;
                string statusComboBox = statuslist[indexstatus]; 
                formElemEditAndCreate = new FormElementEditAndCreate(this, cnn, doc.ID, trybEdit, statusComboBox);
                formElemEditAndCreate.StartPosition = FormStartPosition.Manual;
                formElemEditAndCreate.Left = 0;
                formElemEditAndCreate.Top = 2;
                formElemEditAndCreate.Size = new Size(formDocList.formMain.Size.Width - 20, formDocList.formMain.Size.Height - formDocList.formMain.sizeMenu - 70);
                formElemEditAndCreate.MdiParent = formDocList.formMain;
                this.Visible = false;
                formDocList.formMain.formResize = formElemEditAndCreate;
                formElemEditAndCreate.Show();
            }
            catch (Exception ex)
            {
                try
                {
                    this.Visible = true;
                    formDocList.formMain.formResize = this;
                    formElemEditAndCreate.Close();
                }
                catch { }
                throw new Exception("Błąd podczas ustawiania forma! " + ex.Message);
            }
        }

        public void updateListAndView(Elements elm, int index)
        {
            try
            {
                elemList[index] = elm;
                datagiredview1_Odswierz();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Błąd podczas zmiany w liście! " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void insertListnAndView(Elements elm)
        {
            try
            {
                if (dataGridView1.Rows.Count > 0)
                {
                    List<Elements> newElmList = new List<Elements>();
                    foreach (Elements element in elemList)
                    {
                        newElmList.Add(element);
                    }
                    newElmList.Add(elm);
                    elemList = newElmList;
                    datagiredview1_Odswierz();
                }
                else
                {
                    elemList = new List<Elements>();
                    elemList.Add(elm);
                    datagiredview1_Odswierz();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Bład podczas dodawania do listy! " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public void datagiredview1_Odswierz()
        {
            dataGridView1.DataSource = elemList;
            dataGridView1.Refresh();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedCells.Count == 1)
            {
                try
                {
                    int index = dataGridView1.SelectedCells[0].RowIndex;
                    DataGridViewRow row = dataGridView1.Rows[index];
                    Elements elm = elemList[index];
                    openElmEdit(elm, true, index);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Bład podczas wyświetlania forma! " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

            }
            else
            {
                MessageBox.Show("Wybrano więcej niż jeden element!", "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void openElmEdit(Elements elm, bool trybEdit, int index)
        {

            try
            {
                int indexstatus = comboBox_status.SelectedIndex;
                string statusComboBox = statuslist[indexstatus];
                formElemEditAndCreate = new FormElementEditAndCreate(this, elm, cnn, true,index,statusComboBox);
                formElemEditAndCreate.StartPosition = FormStartPosition.Manual;
                formElemEditAndCreate.Left = 0;
                formElemEditAndCreate.Top = 2;
                formElemEditAndCreate.Size = new Size(formDocList.formMain.Size.Width - 20, formDocList.formMain.Size.Height - formDocList.formMain.sizeMenu - 70);
                formElemEditAndCreate.MdiParent = formDocList.formMain;
                this.Visible = false;
                formDocList.formMain.formResize = formElemEditAndCreate;
                formElemEditAndCreate.Show();
            }
            catch (Exception ex)
            {
                try
                {
                    this.Visible = true;
                    formDocList.formMain.formResize = this;
                    formElemEditAndCreate.Close();
                }
                catch { }
                throw new Exception("Błąd podczas ładownaia danych w oknie! " + ex.Message);
            }
        }

    }
}
