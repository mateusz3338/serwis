﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Serwis
{
    public partial class FormMain : Form
    {
        public Form formResize;
        public int opeid;
        public string imie;
        public string nazwisko;
        public SqlConnection cnn;
        private FormLogin formLogin;
        private FormSprzetList formSprzetList;
        private FormOpeList formOpeList;
        private FormDocList formDocList;
        private FormKNT formknt;
        public bool onFormKnt = false;
        public bool onFormSprzet = false;
        public bool onFormDokumenty = false;
        public bool onFormElementy = false;
        public bool onFormOperatorzy = false;
        public int sizeMenu = 62;
        private bool wyloguj;

        public FormMain(FormLogin formLogin, int opeid, string imie,string nazwisko, SqlConnection cnn)
        {
            this.opeid = opeid;
            this.imie = imie;
            this.nazwisko = nazwisko;
            this.cnn = cnn;
            this.formLogin = formLogin;
            wyloguj = false;
            InitializeComponent();
            Ustaw_LabelZalogowano();
            MdiClient ctlMDI;
            foreach (Control ctl in this.Controls)
            {
                try
                {
                    ctlMDI = (MdiClient)ctl;
                    ctlMDI.BackColor = this.BackColor;
                }
                catch { }
            }
        }

        private void Ustaw_LabelZalogowano()
        {
            int x = label_zalogowano.Location.X + label_zalogowano.Size.Width;
            int y = label_zalogowano.Location.Y;
            label_zalogowano.Text = String.Format("Zalogowany: {0} {1}", imie, nazwisko);
            x = x - label_zalogowano.Size.Width;
            label_zalogowano.Location = new Point(x, y);
            label_zalogowano.Refresh();
        }

        private void FormLogowanie()
        {
            formLogin.textBox_Login.Text = "";
            formLogin.textBox_pass.Text = "";
            formLogin.Show();
        }

        private void FormMain_Load(object sender, EventArgs e)
        {

        }

        private void FormMain_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (wyloguj == true)
                FormLogowanie();
            else
                Exit.CloseAppSQL(cnn);
        }

        private void Button_Wyloguj_Click(object sender, EventArgs e)
        {
            wyloguj = true;
            this.Close();
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            wyloguj = true;
            this.Close();
        }

        private void Button_Knt_Click(object sender, EventArgs e)
        {
            if (onFormKnt == false)
            {
                Close2Form();
                formknt = new FormKNT(sizeMenu, this, cnn);
                formknt.StartPosition = FormStartPosition.Manual;
                formknt.Left = 0;
                formknt.Top = 2;
                formknt.Size = new Size(this.Size.Width - 20, this.Size.Height - sizeMenu - 70);
                formknt.MdiParent = this;
                onFormKnt = true;
                formResize = formknt;
                formknt.Show();
            }
        }

        private void FormMain_Resize(object sender, EventArgs e)
        {
            if (formResize != null)
            {
                formResize.Size = new Size(this.Size.Width - 20, this.Size.Height - sizeMenu - 66);
            }
        }

        private void Close2Form()
        {
            try
            {
                if (onFormKnt)
                {
                    formknt.Close();
                    onFormKnt = false;
                }
                else if (onFormSprzet)
                {
                    formSprzetList.Close();
                    onFormSprzet = false;
                }
                else if (onFormOperatorzy)
                {
                    formOpeList.Close();
                    onFormOperatorzy = false;
                }
                else if(onFormDokumenty)
                {
                    formDocList.Close();
                    onFormDokumenty = false;
                }
            }
            catch(Exception ex)
            {
                throw new Exception("Błąd podczas zamykania Forma! " + ex.Message);
            }
        }

        private void button_sprzet_Click(object sender, EventArgs e)
        {
            if(onFormSprzet == false)
            {
                Close2Form();
                formSprzetList = new FormSprzetList(sizeMenu, this, cnn);
                formSprzetList.StartPosition = FormStartPosition.Manual;
                formSprzetList.Left = 0;
                formSprzetList.Top = 2;
                formSprzetList.Size = new Size(this.Size.Width - 20, this.Size.Height - sizeMenu - 70);
                formSprzetList.MdiParent = this;
                onFormSprzet = true;
                formResize = formSprzetList;
                formSprzetList.Show();
            }
        }

        private void Button_Dokumenty_Click(object sender, EventArgs e)
        {
            if(onFormDokumenty == false)
            {
                Close2Form();
                formDocList = new FormDocList(sizeMenu, this, cnn);
                formDocList.StartPosition = FormStartPosition.Manual;
                formDocList.Left = 0;
                formDocList.Top = 2;
                formDocList.Size = new Size(this.Size.Width - 20, this.Size.Height - sizeMenu - 70);
                formDocList.MdiParent = this;
                onFormDokumenty = true;
                formResize = formDocList;
                formDocList.Show();
            }
        }

        private void button_elementy_Click(object sender, EventArgs e)
        {
            if(onFormElementy == false)
            {
                Close2Form();
            }
        }

        private void button_operatorzu_Click(object sender, EventArgs e)
        {
            if(onFormOperatorzy == false)
            {
                Close2Form();
                formOpeList = new FormOpeList(sizeMenu, this, cnn);
                formOpeList.StartPosition = FormStartPosition.Manual;
                formOpeList.Left = 0;
                formOpeList.Top = 2;
                formOpeList.Size = new Size(this.Size.Width - 20, this.Size.Height - sizeMenu - 70);
                formOpeList.MdiParent = this;
                onFormOperatorzy = true;
                formResize = formOpeList;
                formOpeList.Show();
            }
        }
    }
}
