﻿namespace Serwis
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMain));
            this.label_zalogowano = new System.Windows.Forms.Label();
            this.toolStrip_menu = new System.Windows.Forms.ToolStrip();
            this.button_Wyloguj = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.Button_Knt = new System.Windows.Forms.ToolStripButton();
            this.button_sprzet = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.Button_Dokumenty = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.button_operatorzu = new System.Windows.Forms.ToolStripButton();
            this.toolStrip_menu.SuspendLayout();
            this.SuspendLayout();
            // 
            // label_zalogowano
            // 
            this.label_zalogowano.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label_zalogowano.AutoSize = true;
            this.label_zalogowano.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label_zalogowano.Location = new System.Drawing.Point(733, 546);
            this.label_zalogowano.Name = "label_zalogowano";
            this.label_zalogowano.Size = new System.Drawing.Size(51, 16);
            this.label_zalogowano.TabIndex = 0;
            this.label_zalogowano.Text = "label1";
            // 
            // toolStrip_menu
            // 
            this.toolStrip_menu.AutoSize = false;
            this.toolStrip_menu.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip_menu.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.toolStrip_menu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.button_Wyloguj,
            this.toolStripSeparator1,
            this.Button_Knt,
            this.button_sprzet,
            this.toolStripSeparator2,
            this.Button_Dokumenty,
            this.toolStripSeparator3,
            this.button_operatorzu});
            this.toolStrip_menu.Location = new System.Drawing.Point(0, 0);
            this.toolStrip_menu.Name = "toolStrip_menu";
            this.toolStrip_menu.Size = new System.Drawing.Size(784, 70);
            this.toolStrip_menu.TabIndex = 1;
            this.toolStrip_menu.Text = "Memu";
            // 
            // button_Wyloguj
            // 
            this.button_Wyloguj.AutoSize = false;
            this.button_Wyloguj.AutoToolTip = false;
            this.button_Wyloguj.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.button_Wyloguj.Image = global::Serwis.Properties.Resources.logout50x50;
            this.button_Wyloguj.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.button_Wyloguj.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.button_Wyloguj.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.button_Wyloguj.Name = "button_Wyloguj";
            this.button_Wyloguj.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.button_Wyloguj.Size = new System.Drawing.Size(68, 67);
            this.button_Wyloguj.Text = "Wyloguj";
            this.button_Wyloguj.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.button_Wyloguj.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
            this.button_Wyloguj.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 70);
            // 
            // Button_Knt
            // 
            this.Button_Knt.AutoSize = false;
            this.Button_Knt.AutoToolTip = false;
            this.Button_Knt.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.Button_Knt.Image = global::Serwis.Properties.Resources.Kontrahenci50x50;
            this.Button_Knt.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.Button_Knt.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Button_Knt.Name = "Button_Knt";
            this.Button_Knt.Size = new System.Drawing.Size(68, 67);
            this.Button_Knt.Text = "Kontrahenci";
            this.Button_Knt.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.Button_Knt.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
            this.Button_Knt.Click += new System.EventHandler(this.Button_Knt_Click);
            // 
            // button_sprzet
            // 
            this.button_sprzet.AutoSize = false;
            this.button_sprzet.AutoToolTip = false;
            this.button_sprzet.Image = global::Serwis.Properties.Resources.sprzet42x42;
            this.button_sprzet.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.button_sprzet.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.button_sprzet.Name = "button_sprzet";
            this.button_sprzet.Size = new System.Drawing.Size(68, 67);
            this.button_sprzet.Text = "Sprzęt";
            this.button_sprzet.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.button_sprzet.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
            this.button_sprzet.Click += new System.EventHandler(this.button_sprzet_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 70);
            // 
            // Button_Dokumenty
            // 
            this.Button_Dokumenty.AutoSize = false;
            this.Button_Dokumenty.AutoToolTip = false;
            this.Button_Dokumenty.Image = global::Serwis.Properties.Resources.dokumenty50x50;
            this.Button_Dokumenty.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.Button_Dokumenty.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Button_Dokumenty.Name = "Button_Dokumenty";
            this.Button_Dokumenty.Size = new System.Drawing.Size(68, 67);
            this.Button_Dokumenty.Text = "Dokumenty";
            this.Button_Dokumenty.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.Button_Dokumenty.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
            this.Button_Dokumenty.Click += new System.EventHandler(this.Button_Dokumenty_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 70);
            // 
            // button_operatorzu
            // 
            this.button_operatorzu.AutoSize = false;
            this.button_operatorzu.AutoToolTip = false;
            this.button_operatorzu.Image = global::Serwis.Properties.Resources.Operatorzy50x50;
            this.button_operatorzu.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.button_operatorzu.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.button_operatorzu.Name = "button_operatorzu";
            this.button_operatorzu.Size = new System.Drawing.Size(68, 67);
            this.button_operatorzu.Text = "Operatorzy";
            this.button_operatorzu.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.button_operatorzu.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
            this.button_operatorzu.Click += new System.EventHandler(this.button_operatorzu_Click);
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.Controls.Add(this.toolStrip_menu);
            this.Controls.Add(this.label_zalogowano);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.MinimumSize = new System.Drawing.Size(800, 600);
            this.Name = "FormMain";
            this.Text = "FormMain";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FormMain_FormClosed);
            this.Load += new System.EventHandler(this.FormMain_Load);
            this.Resize += new System.EventHandler(this.FormMain_Resize);
            this.toolStrip_menu.ResumeLayout(false);
            this.toolStrip_menu.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label_zalogowano;
        private System.Windows.Forms.ToolStripButton button_Wyloguj;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton Button_Knt;
        public System.Windows.Forms.ToolStrip toolStrip_menu;
        private System.Windows.Forms.ToolStripButton button_sprzet;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton Button_Dokumenty;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripButton button_operatorzu;
    }
}