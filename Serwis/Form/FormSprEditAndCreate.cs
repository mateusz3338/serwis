﻿using Dapper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Serwis
{
    public partial class FormSprEditAndCreate : Form
    {
        private FormSprzetList formSprList;
        private Sprzet spr;
        private bool trybEdit;
        private int index;
        private SqlConnection cnn;

        public FormSprEditAndCreate(FormSprzetList formSprList, Sprzet spr, SqlConnection cnn, bool trybEdit, int index)
        {
            this.index = index;
            this.trybEdit = trybEdit;
            this.formSprList = formSprList;
            this.cnn = cnn;
            this.spr = spr;
            InitializeComponent();
            UstawDane();

        }
        public FormSprEditAndCreate(FormSprzetList formSprList, SqlConnection cnn, bool trybEdit)
        {
            this.trybEdit = trybEdit;
            this.formSprList = formSprList;
            this.cnn = cnn;
            InitializeComponent();
            label9.Text = "Nowy Sprzęt";
            label_sprNazwa.Visible = false;
        }

        private void FormSprEditAndCreate_Load(object sender, EventArgs e)
        {

        }

        private void UstawDane()
        {
            try
            {
                label_sprNazwa.Text = spr.Nazwa;
                textBox_nazwa.Text = spr.Nazwa;
                textBox_model.Text = spr.Model;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Błąd podczas ładowania danych! " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
            }
        }

        private void button_cancel_Click(object sender, EventArgs e)
        {
            try
            {
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Błąd podczas przełaczenia formów! " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void FormSprEditAndCreate_FormClosed(object sender, FormClosedEventArgs e)
        {
            try
            {
                formSprList.Visible = true;
                formSprList.Left = 0;
                formSprList.Top = 2;
                formSprList.Size = new Size(formSprList.formMain.Size.Width - 20, formSprList.formMain.Size.Height - formSprList.formMain.sizeMenu - 70);
                formSprList.formMain.formResize = formSprList;
                formSprList.formMain.toolStrip_menu.Enabled = true;
            }
            catch(Exception ex)
            {
                MessageBox.Show("Błąd podczas zamykania okna! " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button_ok_Click(object sender, EventArgs e)
        {
            if (textBox_nazwa.Text != "")
            {
                if (trybEdit)
                {

                    try
                    {
                        spr = new Sprzet
                        {
                            ID = spr.ID,
                            Nazwa = textBox_nazwa.Text,
                            Model = textBox_model.Text
                        };
                        UpdateSQL(spr);
                        formSprList.updateListAndView(spr, index);
                        this.Close();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Błąd podczas update! " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    try
                    {
                        spr = new Sprzet
                        {
                            Nazwa = textBox_nazwa.Text,
                            Model = textBox_model.Text
                        };
                        InsertSQL(spr);
                        Sprzet nspr = FindOneSpr(spr.Nazwa, spr.Model);
                        formSprList.insertListnAndView(nspr);
                        this.Close();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Błąd podczas dodawania! " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }

            }
            else
            {
                MessageBox.Show("Nie uzupełniono wszystkich pól!", "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        public Sprzet FindOneSpr(string nazwa, string model)
        {
            try
            {
                IDbConnection db = cnn;
                if (db.State == ConnectionState.Closed)
                    db.Open();
                string sql;
                if (model != "")
                     sql = "select top(1) * from dbo.sprzet where Nazwa = @nazwa and Model = @model order by id desc";
                else
                    sql = "select top(1) * from dbo.sprzet where Nazwa = @nazwa and Model is null order by id desc";
                return db.Query<Sprzet>(sql, new { nazwa, model }).SingleOrDefault();
            }
            catch(Exception ex)
            {
                throw new Exception("Błąd podczas selectcie po insercie! " + ex.Message);
            }
        }

        public void UpdateSQL(Sprzet spr)
        {
            try
            {
                IDbConnection db = cnn;
                if (db.State == ConnectionState.Closed)
                    db.Open();
                string sqlQuery = "update dbo.sprzet set Nazwa = @Nazwa, Model = @Model where id = @id";
                db.Execute(sqlQuery, spr);
            }
            catch(Exception ex)
            {
                throw new Exception("Błąd podczas update! " + ex.Message);
            }
        }

        public void InsertSQL(Sprzet spr)
        {
            try
            {
                IDbConnection db = cnn;
                if (db.State == ConnectionState.Closed)
                    db.Open();
                string sqlQuery = "insert into dbo.sprzet select @Nazwa,(case when @Model <> '' then @Model else null end)";
                db.Execute(sqlQuery, spr);
            }
            catch(Exception ex)
            {
                throw new Exception("Błąd podczas insert! " + ex.Message);
            }
        }
    }
}
