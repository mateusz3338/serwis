﻿using Dapper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Serwis
{
    public partial class FormOpeList : Form
    {

        private int sizeMenu;
        public FormMain formMain;
        private FormOpeEditAndCreate formOpe;
        public SqlConnection cnn;
        private Button button4;
        private Button button3;
        private Button button2;
        private Button button_delete;
        private Label label1;
        private CheckBox checkBox_zwolnieni;
        private Button button_szukaj;
        private TextBox textBox1;
        private DataGridView dataGridView1;
        private Button button1;
        private List<Operatorzy> opelist;

        public FormOpeList(int sizeMenu, FormMain formMain, SqlConnection cnn)
        {
            this.formMain = formMain;
            this.sizeMenu = sizeMenu;
            this.cnn = cnn;
            InitializeComponent();
            try
            {
                opelist = ReadOpeList();
                datagiredview1_Odswierz();
                UstawTabele();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Błąd podczas ładownia tabeli! " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void FormOpeList_Load(object sender, EventArgs e)
        {

        }

        public List<Operatorzy> ReadOpeList()
        {
            string sql = "select * from dbo.operatorzy where DataZwolnienia >= cast(GETDATE() as date) or DataZwolnienia is null";

            try
            {
                IDbConnection db = cnn;
                if (db.State == ConnectionState.Closed)
                    db.Open();
                return db.Query<Operatorzy>(sql).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception("Błąd podczas pobierania danych! " + ex.Message);
            }
        }

        private void UstawTabele()
        {
            try
            {
                dataGridView1.Columns["ID"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                dataGridView1.Columns["ID"].ReadOnly = true;
                dataGridView1.Columns["Imie"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                dataGridView1.Columns["Nazwisko"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                dataGridView1.Columns["Login"].Visible = false;
                dataGridView1.Columns["Haslo"].Visible = false;
                dataGridView1.Columns["DataZatrudnienia"].ReadOnly = true;
                dataGridView1.Columns["DataZatrudnienia"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                dataGridView1.Columns["DataZatrudnienia"].HeaderText = "Data Zatrudnienia";
                dataGridView1.Columns["DataZwolnienia"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                dataGridView1.Columns["DataZwolnienia"].ReadOnly = true;
                dataGridView1.Columns["DataZwolnienia"].HeaderText = "Data Zwolnienia";
                ((DataGridViewTextBoxColumn)dataGridView1.Columns["Imie"]).MaxInputLength = 50;
                ((DataGridViewTextBoxColumn)dataGridView1.Columns["Nazwisko"]).MaxInputLength = 50;
                dataGridView1.Refresh();
            }
            catch (Exception ex)
            {
                throw new Exception("Błąd podczas ustawiania tabeli. " + ex.Message);
            }
        }

        private void button_szukaj_Click(object sender, EventArgs e)
        {
            int czyA = (int)checkBox_zwolnieni.CheckState;
            string text = textBox1.Text;
            try
            {
                opelist = Find(text, czyA);
                datagiredview1_Odswierz();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Błąd podczas szukania! " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public List<Operatorzy> Find(string text, int czyZ)
        {
            try
            {
                IDbConnection db = cnn;
                if (db.State == ConnectionState.Closed)
                    db.Open();
                string sql;
                if (czyZ == 1)
                    sql = "select * from dbo.operatorzy where (Imie like '%'+@text+'%' or Nazwisko like '%'+@text+'%')";
                else
                    sql = "select * from dbo.operatorzy where (Imie like '%'+@text+'%' or Nazwisko like '%'+@text+'%') and (DataZwolnienia >= cast(GETDATE() as date) or DataZwolnienia is null)";
                return db.Query<Operatorzy>(sql, new { text }).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception("Błąd podczas szukania! " + ex.Message);
            }
        }

        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Enter)
            {
                int czyA = (int)checkBox_zwolnieni.CheckState;
                string text = textBox1.Text;
                try
                {
                    opelist = Find(text, czyA);
                    datagiredview1_Odswierz();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Błąd podczas szukania! " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void Delete(List<int> id, List<int> indexs)
        {
            try
            {
                bool czySamegoSiebie = false;
                foreach(int i in id)
                {
                    if (i == formMain.opeid)
                    {
                        czySamegoSiebie = true;
                        break;
                    }
                }
                if (czySamegoSiebie == false)
                {
                    IDbConnection db = cnn;
                    if (db.State == ConnectionState.Closed)
                        db.Open();
                    db.Execute("delete from dbo.operatorzy where id in @id", new { id });
                    indexs.Sort();
                    indexs.Reverse();
                    foreach (int index in indexs)
                    {
                        opelist.RemoveAt(index);
                    }
                    dataGridView1.DataSource = opelist.ToList();
                    dataGridView1.Refresh();
                }
                else
                {
                    MessageBox.Show("Nie można usunąć samego siebie!\nMożna ustawić datę zwolnienia!", "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Błąd podczas usuwania! " + ex.Message);
            }
        }

        public void Update(Operatorzy ope)
        {
            try
            {
                IDbConnection db = cnn;
                if (db.State == ConnectionState.Closed)
                    db.Open();
                string sqlQuery = "update dbo.Operatorzy set Imie = @Imie, Nazwisko = @Nazwisko where id = @id";
                db.Execute(sqlQuery, ope);
            }
            catch (Exception ex)
            {
                throw new Exception("Błąd podczas aktualizacji! " + ex.Message);
            }
        }

        private void dataGridView1_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                Operatorzy ope = opelist[e.RowIndex];
                Update(ope);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Błąd podczas aktualizowania danych! " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button_delete_Click(object sender, EventArgs e)
        {
            try
            {
                List<int> ids = new List<int>();
                List<int> indexs = new List<int>();
                int oldindex = -1;
                foreach (DataGridViewCell c in dataGridView1.SelectedCells)
                {
                    if (c.RowIndex != oldindex)
                    {
                        DataGridViewRow row = dataGridView1.Rows[c.RowIndex];
                        string nazwisko = row.Cells["Nazwisko"].Value.ToString();
                        DialogResult pytanie = MessageBox.Show(String.Format("Czy chcesz usunąć Operatora: {0}?", nazwisko), "Pytanie", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                        if (pytanie == DialogResult.Yes)
                        {
                            indexs.Add(c.RowIndex);
                            ids.Add((int)row.Cells["ID"].Value);
                        }
                    }
                    oldindex = c.RowIndex;
                }
                if (indexs.Count > 0)
                    Delete(ids, indexs);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Błąd podczas usuwania! " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void datagiredview1_Odswierz()
        {
            dataGridView1.DataSource = opelist;
            dataGridView1.Refresh();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            formMain.onFormSprzet = false;
            this.Close();
        }

        private void InitializeComponent()
        {
            this.button4 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button_delete = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.checkBox_zwolnieni = new System.Windows.Forms.CheckBox();
            this.button_szukaj = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.button1 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // button4
            // 
            this.button4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.button4.Image = global::Serwis.Properties.Resources.Add16x16;
            this.button4.Location = new System.Drawing.Point(539, 528);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 25);
            this.button4.TabIndex = 21;
            this.button4.Text = "Dodaj";
            this.button4.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button3
            // 
            this.button3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.button3.Image = global::Serwis.Properties.Resources.Cancle16x16;
            this.button3.Location = new System.Drawing.Point(11, 523);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 25);
            this.button3.TabIndex = 20;
            this.button3.Text = "Zamknij";
            this.button3.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button2
            // 
            this.button2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.button2.Image = global::Serwis.Properties.Resources.edit16x16;
            this.button2.Location = new System.Drawing.Point(458, 528);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 25);
            this.button2.TabIndex = 19;
            this.button2.Text = "Edytuj";
            this.button2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button_delete
            // 
            this.button_delete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button_delete.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.button_delete.Image = global::Serwis.Properties.Resources.Delete16x16;
            this.button_delete.Location = new System.Drawing.Point(377, 528);
            this.button_delete.Name = "button_delete";
            this.button_delete.Size = new System.Drawing.Size(75, 25);
            this.button_delete.TabIndex = 18;
            this.button_delete.Text = "Usuń";
            this.button_delete.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button_delete.UseVisualStyleBackColor = true;
            this.button_delete.Click += new System.EventHandler(this.button_delete_Click);
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(11, 495);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(36, 15);
            this.label1.TabIndex = 17;
            this.label1.Text = "Filtr:";
            // 
            // checkBox_zwolnieni
            // 
            this.checkBox_zwolnieni.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBox_zwolnieni.AutoSize = true;
            this.checkBox_zwolnieni.Location = new System.Drawing.Point(436, 499);
            this.checkBox_zwolnieni.Name = "checkBox_zwolnieni";
            this.checkBox_zwolnieni.Size = new System.Drawing.Size(91, 17);
            this.checkBox_zwolnieni.TabIndex = 16;
            this.checkBox_zwolnieni.Text = "Czy Zwolnieni";
            this.checkBox_zwolnieni.UseVisualStyleBackColor = true;
            // 
            // button_szukaj
            // 
            this.button_szukaj.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button_szukaj.Image = global::Serwis.Properties.Resources.search16x16;
            this.button_szukaj.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button_szukaj.Location = new System.Drawing.Point(539, 494);
            this.button_szukaj.Name = "button_szukaj";
            this.button_szukaj.Size = new System.Drawing.Size(75, 28);
            this.button_szukaj.TabIndex = 15;
            this.button_szukaj.Text = "Szukaj";
            this.button_szukaj.UseVisualStyleBackColor = true;
            this.button_szukaj.Click += new System.EventHandler(this.button_szukaj_Click);
            // 
            // textBox1
            // 
            this.textBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox1.Location = new System.Drawing.Point(53, 494);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(377, 20);
            this.textBox1.TabIndex = 14;
            this.textBox1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBox1_KeyDown);
            // 
            // dataGridView1
            // 
            this.dataGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(12, 12);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(602, 476);
            this.dataGridView1.TabIndex = 13;
            this.dataGridView1.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellEndEdit);
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.Location = new System.Drawing.Point(548, 169);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(67, 23);
            this.button1.TabIndex = 12;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // FormOpeList
            // 
            this.ClientSize = new System.Drawing.Size(627, 561);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button_delete);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.checkBox_zwolnieni);
            this.Controls.Add(this.button_szukaj);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.button1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FormOpeList";
            this.Load += new System.EventHandler(this.FormOpeList_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private void openSprEdit(Operatorzy ope, bool trybEdit, int index)
        {

            try
            {
                formOpe = new FormOpeEditAndCreate(this, ope, cnn, trybEdit, index);
                formOpe.StartPosition = FormStartPosition.Manual;
                formOpe.Left = 0;
                formOpe.Top = 2;
                formOpe.Size = new Size(formMain.Size.Width - 20, formMain.Size.Height - sizeMenu - 70);
                formOpe.MdiParent = formMain;
                this.Visible = false;
                formMain.toolStrip_menu.Enabled = false;
                formMain.formResize = formOpe;
                formOpe.Show();
            }
            catch (Exception ex)
            {
                try
                {
                    this.Visible = true;
                    formMain.toolStrip_menu.Enabled = true;
                    formMain.formResize = this;
                    formOpe.Close();
                }
                catch { }
                throw new Exception("Błąd podczas ładownaia danych w oknie! " + ex.Message);
            }
        }

        public void updateListAndView(Operatorzy ope, int index)
        {
            try
            {
                opelist[index] = ope;
                datagiredview1_Odswierz();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Błąd podczas zmiany w liście! " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void insertListnAndView(Operatorzy ope)
        {
            try
            {
                if (dataGridView1.Rows.Count == 0)
                {
                    List<Operatorzy> newList = new List<Operatorzy>();
                    foreach (Operatorzy o in opelist)
                    {
                        newList.Add(o);
                    }
                    newList.Add(ope);
                    opelist = newList;
                    datagiredview1_Odswierz();
                }
                else
                {
                    opelist.Add(ope);
                    datagiredview1_Odswierz();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Bład podczas dodawania do listy! " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedCells.Count == 1)
            {
                try
                {
                    int index = dataGridView1.SelectedCells[0].RowIndex;
                    DataGridViewRow row = dataGridView1.Rows[index];
                    Operatorzy ope = opelist[index];
                    openSprEdit(ope, true, index);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Bład podczas wyświetlania forma! " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

            }
            else
            {
                MessageBox.Show("Wybrano więcej niż jednego kontrahenta!", "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void openOpeCreate(bool trybEdit)
        {

            try
            {
                formOpe = new FormOpeEditAndCreate(this, cnn, trybEdit);
                formOpe.StartPosition = FormStartPosition.Manual;
                formOpe.Left = 0;
                formOpe.Top = 2;
                formOpe.Size = new Size(formMain.Size.Width - 20, formMain.Size.Height - sizeMenu - 70);
                formOpe.MdiParent = formMain;
                this.Visible = false;
                formMain.toolStrip_menu.Enabled = false;
                formMain.formResize = formOpe;
                formOpe.Show();
            }
            catch (Exception ex)
            {
                try
                {
                    this.Visible = true;
                    formMain.toolStrip_menu.Enabled = true;
                    formMain.formResize = this;
                    formOpe.Close();
                }
                catch { }
                throw new Exception("Błąd podczas ustawiania forma! " + ex.Message);
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            try
            {
                openOpeCreate(false);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Błąd podczas uruchamiania forma " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
