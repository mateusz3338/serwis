﻿using Dapper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Serwis
{
    public partial class FormDocList : Form
    {
        private int sizeMenu;
        public FormMain formMain;
        private FormDokumentEditAndCreate formDoc;
        public SqlConnection cnn;
        private List<Dokument> doclist;

        public FormDocList(int sizeMenu, FormMain formMain, SqlConnection cnn)
        {
            this.formMain = formMain;
            this.sizeMenu = sizeMenu;
            this.cnn = cnn;
            InitializeComponent();
            try
            {
                doclist = new List<Dokument>();
                doclist = ReadDocList();
                datagiredview1_Odswierz();
                UstawTabele();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Błąd podczas ładownia tabeli! " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void FormDocList_Load(object sender, EventArgs e)
        {

        }

        public List<Dokument> ReadDocList()
        {
            string sql = "select d.id,k.id as ID_KNT, o.id as ID_OPE, k.Akronim as Kontrahent,o.Nazwisko + ' ' + o.Imie as Operator, DataZgloszenia, Numer, case when Status = 1 then 'Bufor' when Status = 2 then 'Zatwierdzone' when Status = 3 then 'W Realizacji' when Status = 4 then 'Zakończone' when Status = 5 then 'Anulowane' end as Status from dbo.Documents as d join dbo.kontrahenci as k on k.ID = d.ID_KNT join dbo.Operatorzy as o on o.id = d.ID_Ope";

            try
            {
                IDbConnection db = cnn;
                if (db.State == ConnectionState.Closed)
                    db.Open();
                return db.Query<Dokument>(sql).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception("Błąd podczas pobierania danych! " + ex.Message);
            }
        }

        private void UstawTabele()
        {
            try
            {
                dataGridView1.Columns["ID"].Visible = false;
                dataGridView1.Columns["ID_KNT"].Visible = false;
                dataGridView1.Columns["ID_OPE"].Visible = false;
                dataGridView1.Columns["DataZgloszenia"].HeaderText = "Data Zgłoszenia";

                dataGridView1.Columns["Kontrahent"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                dataGridView1.Columns["Operator"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                dataGridView1.Columns["Numer"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                dataGridView1.Columns["DataZgloszenia"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                dataGridView1.Columns["Status"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;

                dataGridView1.Columns["DataZgloszenia"].ReadOnly = true;
                dataGridView1.Columns["Operator"].ReadOnly = true;
                dataGridView1.Columns["Kontrahent"].ReadOnly = true;
                dataGridView1.Columns["Numer"].ReadOnly = true;
                dataGridView1.Columns["Status"].ReadOnly = true;
                dataGridView1.Refresh();
            }
            catch (Exception ex)
            {
                throw new Exception("Błąd podczas ustawiania tabeli. " + ex.Message);
            }
        }

        public void datagiredview1_Odswierz()
        {
            dataGridView1.DataSource = doclist;
            dataGridView1.Refresh();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            formMain.onFormDokumenty = false;
            this.Close();
        }

        public void updateListAndView(Dokument dok, int index)
        {
            try
            {
                doclist[index] = dok;
                datagiredview1_Odswierz();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Błąd podczas zmiany w liście! " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void AktualizujDoc()
        {
            string text = textBox1.Text;
            try
            {
                doclist = Find(text);
                datagiredview1_Odswierz();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Błąd podczas Aktualizowania! " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public int insertListnAndView(Dokument dok)
        {
            try
            {
                if (dataGridView1.Rows.Count == 0)
                {
                    List<Dokument> newDokList = new List<Dokument>();
                    foreach (Dokument dokument in doclist)
                    {
                        newDokList.Add(dokument);
                    }
                    newDokList.Add(dok);
                    doclist = newDokList;
                    datagiredview1_Odswierz();
                }
                else
                {
                    doclist.Add(dok);
                    datagiredview1_Odswierz();
                }
                return dataGridView1.Rows.Count - 1;
            }
            catch (Exception ex)
            {
                throw new Exception ("Bład podczas dodawania do listy! " + ex.Message);
            }
        }

        public List<Dokument> Find(string text)
        {
            try
            {
                IDbConnection db = cnn;
                if (db.State == ConnectionState.Closed)
                    db.Open();
                string sql = "select d.id,k.id as ID_KNT, o.id as ID_OPE, k.Akronim as Kontrahent,o.Nazwisko + ' ' + o.Imie as Operator, DataZgloszenia, Numer, case when Status = 1 then 'Bufor' when Status = 2 then 'Zatwierdzone' when Status = 3 then 'W Realizacji' when Status = 4 then 'Zakończone' when Status = 5 then 'Anulowane' end as Status from dbo.Documents as d join dbo.kontrahenci as k on k.ID = d.ID_KNT join dbo.Operatorzy as o on o.id = d.ID_Ope where k.Akronim like '%'+@text+'%' or o.Nazwisko + ' ' + o.Imie like '%'+@text+'%' or Numer like '%'+@text+'%'";
                return db.Query<Dokument>(sql, new { text }).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception("Błąd podczas szukania! " + ex.Message);
            }
        }

        private void button_szukaj_Click(object sender, EventArgs e)
        {
            string text = textBox1.Text;
            try
            {
                doclist = Find(text);
                datagiredview1_Odswierz();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Błąd podczas szukania! " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Enter)
            {
                string text = textBox1.Text;
                try
                {
                    doclist = Find(text);
                    datagiredview1_Odswierz();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Błąd podczas szukania! " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void Delete(List<int> id, List<int> indexs)
        {
            try
            {

                IDbConnection db = cnn;
                if (db.State == ConnectionState.Closed)
                    db.Open();
                db.Execute("delete from dbo.elements where id_doc in @id", new { id });
                db.Execute("delete from dbo.documents where id in @id", new { id });
                indexs.Sort();
                indexs.Reverse();
                foreach (int index in indexs)
                {
                    doclist.RemoveAt(index);
                }
                dataGridView1.DataSource = doclist.ToList();
                dataGridView1.Refresh();
            }
            catch (Exception ex)
            {
                throw new Exception("Błąd podczas usuwania! " + ex.Message);
            }
        }

        private void button_delete_Click(object sender, EventArgs e)
        {
            try
            {
                List<int> ids = new List<int>();
                List<int> indexs = new List<int>();
                int oldindex = -1;
                foreach (DataGridViewCell c in dataGridView1.SelectedCells)
                {
                    if (c.RowIndex != oldindex)
                    {
                        DataGridViewRow row = dataGridView1.Rows[c.RowIndex];
                        string numer = row.Cells["Numer"].Value.ToString();
                        string status = row.Cells["Status"].Value.ToString();
                        if (status == "W Realizacji" || status == "Zakończone")
                        {
                            MessageBox.Show(String.Format("Nie można usunąć dokumentu {0} ponieważ jest on zakończony lub w realizacji!", "Info", MessageBoxButtons.OK, MessageBoxIcon.Information));
                        }
                        else
                        {
                            DialogResult pytanie = MessageBox.Show(String.Format("Czy chcesz usunąć dokument: {0}?", numer), "Pytanie", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                            if (pytanie == DialogResult.Yes)
                            {
                                indexs.Add(c.RowIndex);
                                ids.Add((int)row.Cells["ID"].Value);
                            }
                        }
                    }
                    oldindex = c.RowIndex;
                }
                if (indexs.Count > 0)
                    Delete(ids, indexs);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Błąd podczas usuwania! " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void openDocEdit(Dokument dok, bool trybEdit, int index)
        {

            try
            {
                formDoc = new FormDokumentEditAndCreate(this, dok, cnn, trybEdit, index);
                formDoc.StartPosition = FormStartPosition.Manual;
                formDoc.Left = 0;
                formDoc.Top = 2;
                formDoc.Size = new Size(formMain.Size.Width - 20, formMain.Size.Height - sizeMenu - 70);
                formDoc.MdiParent = formMain;
                this.Visible = false;
                formMain.toolStrip_menu.Enabled = false;
                formMain.formResize = formDoc;
                formDoc.Show();
            }
            catch (Exception ex)
            {
                try
                {
                    this.Visible = true;
                    formMain.toolStrip_menu.Enabled = true;
                    formMain.formResize = this;
                    formDoc.Close();
                }
                catch { }
                throw new Exception("Błąd podczas ładownaia danych w oknie! " + ex.Message);
            }
        }

        private void openOpeCreate(bool trybEdit)
        {

            try
            {
                formDoc = new FormDokumentEditAndCreate(this, cnn, trybEdit);
                formDoc.StartPosition = FormStartPosition.Manual;
                formDoc.Left = 0;
                formDoc.Top = 2;
                formDoc.Size = new Size(formMain.Size.Width - 20, formMain.Size.Height - sizeMenu - 70);
                formDoc.MdiParent = formMain;
                this.Visible = false;
                formMain.toolStrip_menu.Enabled = false;
                formMain.formResize = formDoc;
                formDoc.Show();
            }
            catch (Exception ex)
            {
                try
                {
                    this.Visible = true;
                    formMain.toolStrip_menu.Enabled = true;
                    formMain.formResize = this;
                    formDoc.Close();
                }
                catch { }
                throw new Exception("Błąd podczas ustawiania forma! " + ex.Message);
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            try
            {
                openOpeCreate(false);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Błąd podczas uruchamiania forma " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedCells.Count == 1)
            {
                try
                {
                    int index = dataGridView1.SelectedCells[0].RowIndex;
                    DataGridViewRow row = dataGridView1.Rows[index];
                    Dokument doc = doclist[index];
                    openDocEdit(doc, true, index);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Bład podczas wyświetlania forma! " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

            }
            else
            {
                MessageBox.Show("Wybrano więcej niż jednego Dokument!", "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }



        
    }
}
