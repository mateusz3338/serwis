﻿namespace Serwis
{
    partial class FormElementEditAndCreate
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.checkBox_dataZakonczenia = new System.Windows.Forms.CheckBox();
            this.dateTimePicker_dataOdbioru = new System.Windows.Forms.DateTimePicker();
            this.checkBox_dataOdbioru = new System.Windows.Forms.CheckBox();
            this.checkBox_CzyNaprawiono = new System.Windows.Forms.CheckBox();
            this.numericUpDown_koszt = new System.Windows.Forms.NumericUpDown();
            this.label8 = new System.Windows.Forms.Label();
            this.numericUpDown_wycena = new System.Windows.Forms.NumericUpDown();
            this.label7 = new System.Windows.Forms.Label();
            this.richTextBox_OpiszNaprawy = new System.Windows.Forms.RichTextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.richTextBox_OpisUszkodzenia = new System.Windows.Forms.RichTextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.comboBox_SPR = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.dateTimePicker_dataZakonczenia = new System.Windows.Forms.DateTimePicker();
            this.dateTimePicker_DataPrzejecia = new System.Windows.Forms.DateTimePicker();
            this.label_elmLP = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.textBox_OPEWYK = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.button_cancel = new System.Windows.Forms.Button();
            this.button_ok = new System.Windows.Forms.Button();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_koszt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_wycena)).BeginInit();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 90F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.tableLayoutPanel1.Controls.Add(this.panel1, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.panel2, 1, 2);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(12, 12);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 85F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(716, 421);
            this.tableLayoutPanel1.TabIndex = 3;
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.Controls.Add(this.checkBox_dataZakonczenia);
            this.panel1.Controls.Add(this.dateTimePicker_dataOdbioru);
            this.panel1.Controls.Add(this.checkBox_dataOdbioru);
            this.panel1.Controls.Add(this.checkBox_CzyNaprawiono);
            this.panel1.Controls.Add(this.numericUpDown_koszt);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.numericUpDown_wycena);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.richTextBox_OpiszNaprawy);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.richTextBox_OpisUszkodzenia);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.comboBox_SPR);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.dateTimePicker_dataZakonczenia);
            this.panel1.Controls.Add(this.dateTimePicker_DataPrzejecia);
            this.panel1.Controls.Add(this.label_elmLP);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.textBox_OPEWYK);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Location = new System.Drawing.Point(38, 24);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(638, 351);
            this.panel1.TabIndex = 0;
            // 
            // checkBox_dataZakonczenia
            // 
            this.checkBox_dataZakonczenia.AutoSize = true;
            this.checkBox_dataZakonczenia.Enabled = false;
            this.checkBox_dataZakonczenia.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.checkBox_dataZakonczenia.Location = new System.Drawing.Point(260, 276);
            this.checkBox_dataZakonczenia.Name = "checkBox_dataZakonczenia";
            this.checkBox_dataZakonczenia.Size = new System.Drawing.Size(125, 19);
            this.checkBox_dataZakonczenia.TabIndex = 48;
            this.checkBox_dataZakonczenia.Text = "Data Zakonczenia";
            this.checkBox_dataZakonczenia.UseVisualStyleBackColor = true;
            this.checkBox_dataZakonczenia.CheckedChanged += new System.EventHandler(this.checkBox_dataZakonczenia_CheckedChanged);
            // 
            // dateTimePicker_dataOdbioru
            // 
            this.dateTimePicker_dataOdbioru.CustomFormat = "";
            this.dateTimePicker_dataOdbioru.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.dateTimePicker_dataOdbioru.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePicker_dataOdbioru.Location = new System.Drawing.Point(114, 302);
            this.dateTimePicker_dataOdbioru.Name = "dateTimePicker_dataOdbioru";
            this.dateTimePicker_dataOdbioru.Size = new System.Drawing.Size(137, 20);
            this.dateTimePicker_dataOdbioru.TabIndex = 47;
            this.dateTimePicker_dataOdbioru.Value = new System.DateTime(2018, 12, 18, 21, 41, 57, 0);
            this.dateTimePicker_dataOdbioru.Visible = false;
            // 
            // checkBox_dataOdbioru
            // 
            this.checkBox_dataOdbioru.AutoSize = true;
            this.checkBox_dataOdbioru.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.checkBox_dataOdbioru.Location = new System.Drawing.Point(3, 305);
            this.checkBox_dataOdbioru.Name = "checkBox_dataOdbioru";
            this.checkBox_dataOdbioru.Size = new System.Drawing.Size(99, 19);
            this.checkBox_dataOdbioru.TabIndex = 46;
            this.checkBox_dataOdbioru.Text = "Data Odbioru";
            this.checkBox_dataOdbioru.UseVisualStyleBackColor = true;
            this.checkBox_dataOdbioru.CheckedChanged += new System.EventHandler(this.checkBox_dataOdbioru_CheckedChanged);
            // 
            // checkBox_CzyNaprawiono
            // 
            this.checkBox_CzyNaprawiono.AutoSize = true;
            this.checkBox_CzyNaprawiono.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.checkBox_CzyNaprawiono.Location = new System.Drawing.Point(260, 305);
            this.checkBox_CzyNaprawiono.Name = "checkBox_CzyNaprawiono";
            this.checkBox_CzyNaprawiono.Size = new System.Drawing.Size(115, 19);
            this.checkBox_CzyNaprawiono.TabIndex = 44;
            this.checkBox_CzyNaprawiono.Text = "Czy Naprawiono";
            this.checkBox_CzyNaprawiono.UseVisualStyleBackColor = true;
            // 
            // numericUpDown_koszt
            // 
            this.numericUpDown_koszt.Location = new System.Drawing.Point(259, 249);
            this.numericUpDown_koszt.Maximum = new decimal(new int[] {
            2147483647,
            0,
            0,
            0});
            this.numericUpDown_koszt.Name = "numericUpDown_koszt";
            this.numericUpDown_koszt.Size = new System.Drawing.Size(120, 20);
            this.numericUpDown_koszt.TabIndex = 41;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label8.Location = new System.Drawing.Point(207, 249);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(40, 15);
            this.label8.TabIndex = 40;
            this.label8.Text = "Koszt:";
            // 
            // numericUpDown_wycena
            // 
            this.numericUpDown_wycena.Location = new System.Drawing.Point(65, 249);
            this.numericUpDown_wycena.Maximum = new decimal(new int[] {
            2147483647,
            0,
            0,
            0});
            this.numericUpDown_wycena.Name = "numericUpDown_wycena";
            this.numericUpDown_wycena.Size = new System.Drawing.Size(120, 20);
            this.numericUpDown_wycena.TabIndex = 39;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label7.Location = new System.Drawing.Point(3, 249);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(53, 15);
            this.label7.TabIndex = 38;
            this.label7.Text = "Wycena:";
            // 
            // richTextBox_OpiszNaprawy
            // 
            this.richTextBox_OpiszNaprawy.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.richTextBox_OpiszNaprawy.Location = new System.Drawing.Point(112, 173);
            this.richTextBox_OpiszNaprawy.Name = "richTextBox_OpiszNaprawy";
            this.richTextBox_OpiszNaprawy.Size = new System.Drawing.Size(523, 70);
            this.richTextBox_OpiszNaprawy.TabIndex = 37;
            this.richTextBox_OpiszNaprawy.Text = "";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label6.Location = new System.Drawing.Point(3, 176);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(92, 15);
            this.label6.TabIndex = 36;
            this.label6.Text = "Opisz Naprawy:";
            // 
            // richTextBox_OpisUszkodzenia
            // 
            this.richTextBox_OpisUszkodzenia.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.richTextBox_OpisUszkodzenia.Location = new System.Drawing.Point(135, 97);
            this.richTextBox_OpisUszkodzenia.Name = "richTextBox_OpisUszkodzenia";
            this.richTextBox_OpisUszkodzenia.Size = new System.Drawing.Size(500, 70);
            this.richTextBox_OpisUszkodzenia.TabIndex = 35;
            this.richTextBox_OpisUszkodzenia.Text = "";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label4.Location = new System.Drawing.Point(3, 100);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(126, 15);
            this.label4.TabIndex = 34;
            this.label4.Text = "Opisz Uszodzenia:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(3, 277);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(90, 15);
            this.label1.TabIndex = 32;
            this.label1.Text = "Data Przejęcia:";
            // 
            // comboBox_SPR
            // 
            this.comboBox_SPR.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBox_SPR.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_SPR.FormattingEnabled = true;
            this.comboBox_SPR.Location = new System.Drawing.Point(52, 42);
            this.comboBox_SPR.Name = "comboBox_SPR";
            this.comboBox_SPR.Size = new System.Drawing.Size(583, 21);
            this.comboBox_SPR.TabIndex = 31;
            this.comboBox_SPR.DropDown += new System.EventHandler(this.comboBox_SPR_DropDown);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label5.Location = new System.Drawing.Point(3, 44);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(52, 15);
            this.label5.TabIndex = 30;
            this.label5.Text = "Sprzet:";
            // 
            // dateTimePicker_dataZakonczenia
            // 
            this.dateTimePicker_dataZakonczenia.CustomFormat = "yyyy-MM-dd";
            this.dateTimePicker_dataZakonczenia.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePicker_dataZakonczenia.ImeMode = System.Windows.Forms.ImeMode.On;
            this.dateTimePicker_dataZakonczenia.Location = new System.Drawing.Point(387, 273);
            this.dateTimePicker_dataZakonczenia.MinDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateTimePicker_dataZakonczenia.Name = "dateTimePicker_dataZakonczenia";
            this.dateTimePicker_dataZakonczenia.Size = new System.Drawing.Size(137, 20);
            this.dateTimePicker_dataZakonczenia.TabIndex = 27;
            this.dateTimePicker_dataZakonczenia.Value = new System.DateTime(2018, 12, 18, 21, 41, 51, 0);
            this.dateTimePicker_dataZakonczenia.Visible = false;
            // 
            // dateTimePicker_DataPrzejecia
            // 
            this.dateTimePicker_DataPrzejecia.CustomFormat = "";
            this.dateTimePicker_DataPrzejecia.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.dateTimePicker_DataPrzejecia.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePicker_DataPrzejecia.Location = new System.Drawing.Point(114, 273);
            this.dateTimePicker_DataPrzejecia.Name = "dateTimePicker_DataPrzejecia";
            this.dateTimePicker_DataPrzejecia.Size = new System.Drawing.Size(137, 20);
            this.dateTimePicker_DataPrzejecia.TabIndex = 26;
            this.dateTimePicker_DataPrzejecia.Value = new System.DateTime(2018, 12, 18, 21, 41, 57, 0);
            // 
            // label_elmLP
            // 
            this.label_elmLP.AutoSize = true;
            this.label_elmLP.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label_elmLP.Location = new System.Drawing.Point(69, 4);
            this.label_elmLP.Name = "label_elmLP";
            this.label_elmLP.Size = new System.Drawing.Size(45, 16);
            this.label_elmLP.TabIndex = 17;
            this.label_elmLP.Text = "elmLp";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label9.Location = new System.Drawing.Point(6, 4);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(68, 16);
            this.label9.TabIndex = 16;
            this.label9.Text = "Element:";
            // 
            // textBox_OPEWYK
            // 
            this.textBox_OPEWYK.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox_OPEWYK.Enabled = false;
            this.textBox_OPEWYK.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBox_OPEWYK.ImeMode = System.Windows.Forms.ImeMode.On;
            this.textBox_OPEWYK.Location = new System.Drawing.Point(146, 69);
            this.textBox_OPEWYK.MaxLength = 50;
            this.textBox_OPEWYK.Name = "textBox_OPEWYK";
            this.textBox_OPEWYK.Size = new System.Drawing.Size(489, 22);
            this.textBox_OPEWYK.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label2.Location = new System.Drawing.Point(3, 72);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(125, 15);
            this.label2.TabIndex = 2;
            this.label2.Text = "Operator Wykonujący:";
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.Controls.Add(this.button_cancel);
            this.panel2.Controls.Add(this.button_ok);
            this.panel2.Location = new System.Drawing.Point(38, 381);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(638, 37);
            this.panel2.TabIndex = 1;
            // 
            // button_cancel
            // 
            this.button_cancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button_cancel.Image = global::Serwis.Properties.Resources.Cancle16x16;
            this.button_cancel.Location = new System.Drawing.Point(459, 8);
            this.button_cancel.Name = "button_cancel";
            this.button_cancel.Size = new System.Drawing.Size(85, 26);
            this.button_cancel.TabIndex = 26;
            this.button_cancel.Text = "Anuluj";
            this.button_cancel.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button_cancel.UseVisualStyleBackColor = true;
            this.button_cancel.Click += new System.EventHandler(this.button_cancel_Click);
            // 
            // button_ok
            // 
            this.button_ok.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button_ok.Image = global::Serwis.Properties.Resources.Add16x16;
            this.button_ok.Location = new System.Drawing.Point(550, 8);
            this.button_ok.Name = "button_ok";
            this.button_ok.Size = new System.Drawing.Size(85, 26);
            this.button_ok.TabIndex = 25;
            this.button_ok.Text = "Zatwierdź";
            this.button_ok.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button_ok.UseVisualStyleBackColor = true;
            this.button_ok.Click += new System.EventHandler(this.button_ok_Click);
            // 
            // FormElementEditAndCreate
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(740, 445);
            this.Controls.Add(this.tableLayoutPanel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FormElementEditAndCreate";
            this.Text = "FormElementEditAndCreate";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FormElementEditAndCreate_FormClosed);
            this.Load += new System.EventHandler(this.FormElementEditAndCreate_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_koszt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_wycena)).EndInit();
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ComboBox comboBox_SPR;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DateTimePicker dateTimePicker_dataZakonczenia;
        private System.Windows.Forms.DateTimePicker dateTimePicker_DataPrzejecia;
        private System.Windows.Forms.Label label_elmLP;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox textBox_OPEWYK;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button button_cancel;
        private System.Windows.Forms.Button button_ok;
        private System.Windows.Forms.CheckBox checkBox_CzyNaprawiono;
        private System.Windows.Forms.NumericUpDown numericUpDown_koszt;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.NumericUpDown numericUpDown_wycena;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.RichTextBox richTextBox_OpiszNaprawy;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.RichTextBox richTextBox_OpisUszkodzenia;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker dateTimePicker_dataOdbioru;
        private System.Windows.Forms.CheckBox checkBox_dataOdbioru;
        private System.Windows.Forms.CheckBox checkBox_dataZakonczenia;
    }
}