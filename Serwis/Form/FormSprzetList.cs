﻿using Dapper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Serwis
{

    public partial class FormSprzetList : Form
    {
        private int sizeMenu;
        public FormMain formMain;
        public FormSprEditAndCreate formSpr;
        public SqlConnection cnn;
        private List<Sprzet> sprlist;

        public FormSprzetList(int sizeMenu, FormMain formMain, SqlConnection cnn)
        {
            this.formMain = formMain;
            this.sizeMenu = sizeMenu;
            this.cnn = cnn;
            InitializeComponent();
            try
            {
                sprlist = ReadSprList();
                datagiredview1_Odswierz();
                UstawTabele();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Błąd podczas ładownia tabeli! " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public List<Sprzet> ReadSprList()
        {
            string sql = "select * from dbo.sprzet";

            try
            {
                IDbConnection db = cnn;
                if (db.State == ConnectionState.Closed)
                    db.Open();
                return db.Query<Sprzet>(sql).ToList();
            }
            catch(Exception ex)
            {
                throw new Exception("Błąd podczas pobierania danych! " + ex.Message);
            }

        }

        private void UstawTabele()
        {
            try
            {
                dataGridView1.Columns["ID"].Visible = false;
                dataGridView1.Columns["Nazwa"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                dataGridView1.Columns["Model"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                ((DataGridViewTextBoxColumn)dataGridView1.Columns["Nazwa"]).MaxInputLength = 50;
                ((DataGridViewTextBoxColumn)dataGridView1.Columns["Model"]).MaxInputLength = 50;
                dataGridView1.Refresh();
            }
            catch (Exception ex)
            {
                throw new Exception("Błąd podczas ustawiania tabeli. " + ex.Message);
            }
        }

        private void button_szukaj_Click(object sender, EventArgs e)
        {
            string text = textBox1.Text;
            try
            {
                sprlist = Find(text);
                datagiredview1_Odswierz();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Błąd podczas szukania! " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public List<Sprzet> Find(string text)
        {
            try
            {
                IDbConnection db = cnn;
                if (db.State == ConnectionState.Closed)
                    db.Open();
                string sql = "select * from dbo.sprzet where model like '%'+@text+'%' or Nazwa like '%'+@text+'%'";
                return db.Query<Sprzet>(sql, new { text }).ToList();
            }
            catch(Exception ex)
            {
                throw new Exception("Błąd podczas szukania! " + ex.Message);
            }
        }

        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Enter)
            {
                string text = textBox1.Text;
                try
                {
                    sprlist = Find(text);
                    datagiredview1_Odswierz();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Błąd podczas szukania! " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void Delete(List<int> id, List<int> indexs)
        {
            try
            {
                IDbConnection db = cnn;
                if (db.State == ConnectionState.Closed)
                    db.Open();
                db.Execute("delete from dbo.sprzet where id in @id", new { id });
                indexs.Sort();
                indexs.Reverse();
                foreach (int index in indexs)
                {
                    sprlist.RemoveAt(index);
                }
                dataGridView1.DataSource = sprlist.ToList();
                dataGridView1.Refresh();
            }
            catch(Exception ex)
            {
                throw new Exception("Błąd podczas usuwania! " + ex.Message);
            }
        }

        public void Update(Sprzet spr)
        {
            try
            {
                IDbConnection db = cnn;
                if (db.State == ConnectionState.Closed)
                    db.Open();
                string sqlQuery = "update dbo.sprzet set Nazwa = @Nazwa, Model = @Model where id = @id";
                db.Execute(sqlQuery, spr);
            }
            catch(Exception ex)
            {
                throw new Exception("Błąd podczas aktualizacji! " + ex.Message);
            }
        }

        private void dataGridView1_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                Sprzet spr = sprlist[e.RowIndex];
                Update(spr);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Błąd podczas aktualizowania danych! " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button_delete_Click(object sender, EventArgs e)
        {
            try
            {
                List<int> ids = new List<int>();
                List<int> indexs = new List<int>();
                int oldindex = -1;
                foreach (DataGridViewCell c in dataGridView1.SelectedCells)
                {
                    if (c.RowIndex != oldindex)
                    {
                        DataGridViewRow row = dataGridView1.Rows[c.RowIndex];
                        string nazwa = row.Cells["Nazwa"].Value.ToString();
                        DialogResult pytanie = MessageBox.Show(String.Format("Czy chcesz usunąć sprzęt: {0}?", nazwa), "Pytanie", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                        if (pytanie == DialogResult.Yes)
                        {
                            indexs.Add(c.RowIndex);
                            ids.Add((int)row.Cells["ID"].Value);
                        }
                    }
                    oldindex = c.RowIndex;
                }
                if (indexs.Count > 0)
                    Delete(ids, indexs);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Błąd podczas usuwania! " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void datagiredview1_Odswierz()
        {
            dataGridView1.DataSource = sprlist;
            dataGridView1.Refresh();
        }

        private void button_wyjdz_Click(object sender, EventArgs e)
        {
            formMain.onFormSprzet = false;
            this.Close();
        }

        private void openSprEdit(Sprzet spr, bool trybEdit, int index)
        {

            try
            {
                formSpr = new FormSprEditAndCreate(this, spr, cnn, trybEdit, index);
                formSpr.StartPosition = FormStartPosition.Manual;
                formSpr.Left = 0;
                formSpr.Top = 2;
                formSpr.Size = new Size(formMain.Size.Width - 20, formMain.Size.Height - sizeMenu - 70);
                formSpr.MdiParent = formMain;
                this.Visible = false;
                formMain.toolStrip_menu.Enabled = false;
                formMain.formResize = formSpr;
                formSpr.Show();
            }
            catch (Exception ex)
            {
                try
                {
                    this.Visible = true;
                    formMain.toolStrip_menu.Enabled = true;
                    formMain.formResize = this;
                    formSpr.Close();
                }
                catch { }
                throw new Exception("Błąd podczas ładownaia danych w oknie! " + ex.Message);
            }
        }

        public void updateListAndView(Sprzet spr, int index)
        {
            try
            {
                sprlist[index] = spr;
                datagiredview1_Odswierz();
            }
            catch(Exception ex)
            {
                MessageBox.Show("Błąd podczas zmiany w liście! " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void insertListnAndView(Sprzet spr)
        {
            try
            {
                if (dataGridView1.Rows.Count == 0)
                {
                    List<Sprzet> newList = new List<Sprzet>();
                    foreach (Sprzet s in sprlist)
                    {
                        newList.Add(s);
                    }
                    newList.Add(spr);
                    sprlist = newList;
                    datagiredview1_Odswierz();
                }
                else
                {
                    sprlist.Add(spr);
                    datagiredview1_Odswierz();
                }

            }
            catch(Exception ex)
            {
                MessageBox.Show("Bład podczas dodawania do listy! " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button_edytuj_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedCells.Count == 1)
            {
                try
                {
                    int index = dataGridView1.SelectedCells[0].RowIndex;
                    DataGridViewRow row = dataGridView1.Rows[index];
                    Sprzet spr = sprlist[index];
                    openSprEdit(spr, true, index);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Bład podczas wyświetlania forma! " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

            }
            else
            {
                MessageBox.Show("Wybrano więcej niż jednego kontrahenta!", "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void openSprCreate(bool trybEdit)
        {

            try
            {
                formSpr = new FormSprEditAndCreate(this, cnn, trybEdit);
                formSpr.StartPosition = FormStartPosition.Manual;
                formSpr.Left = 0;
                formSpr.Top = 2;
                formSpr.Size = new Size(formMain.Size.Width - 20, formMain.Size.Height - sizeMenu - 70);
                formSpr.MdiParent = formMain;
                this.Visible = false;
                formMain.toolStrip_menu.Enabled = false;
                formMain.formResize = formSpr;
                formSpr.Show();
            }
            catch (Exception ex)
            {
                try
                {
                    this.Visible = true;
                    formMain.toolStrip_menu.Enabled = true;
                    formMain.formResize = this;
                    formSpr.Close();
                }
                catch { }
                throw new Exception("Błąd podczas ustawiania forma! " + ex.Message);
            }
        }

        private void button_dodaj_Click(object sender, EventArgs e)
        {
            try
            {
                openSprCreate(false);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Błąd podczas uruchamiania forma " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void FormSprzetList_Load(object sender, EventArgs e)
        {

        }
    }
}
