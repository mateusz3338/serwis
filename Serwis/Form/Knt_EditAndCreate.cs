﻿using Dapper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Serwis
{
    public partial class Knt_EditAndCreate : Form
    {
        private FormKNT formknt;
        private Knt_AllDane knt;
        private bool trybEdit;
        private int index;
        private SqlConnection cnn;
        public bool zaktualizowano { get; set; }
        public Knt nknt { get; set; }
        public Knt_EditAndCreate(FormKNT formknt, Knt_AllDane knt, SqlConnection cnn,bool trybEdit, int index)
        {
            zaktualizowano = false;
            this.index = index;
            this.trybEdit = trybEdit;
            this.formknt = formknt;
            this.cnn = cnn;
            this.knt = knt;
            InitializeComponent();
            UstawDane();

        }

        public Knt_EditAndCreate(FormKNT formknt, SqlConnection cnn, bool trybEdit)
        {
            zaktualizowano = false;

            this.trybEdit = trybEdit;
            this.formknt = formknt;
            this.cnn = cnn;
            InitializeComponent();
            label9.Text = "Nowy Kontrahent";
            label_kntAkronim.Visible = false;

        }

        private void UstawDane()
        {
            try
            {
                label_kntAkronim.Text = knt.Akronim;
                textBox_akrnoim.Text = knt.Akronim;
                textBox_nazwa1.Text = knt.Nazwa1;
                textBox_nazwa2.Text = knt.Nazwa2;
                textBox_nazwa3.Text = knt.Nazwa3;
                textBox_nip.Text = knt.NIP;
                textBox_miejscowosc.Text = knt.Miejscowosc;
                textBox_kod.Text = knt.KodPocztowy;
                textBox_ulica.Text = knt.Ulica;
                textBox_numerDomu.Text = knt.NumerDomu;
                textBox_numerLokalu.Text = knt.NumerLokum;
                textBox_kontakt.Text = knt.Kontakt;
                if(knt.CzyArchiwalny == 1)
                    checkBox1.CheckState = CheckState.Checked;
            }
            catch(Exception ex)
            {
                MessageBox.Show("Błąd podczas ładowania danych! " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
            }
        }

        private void Knt_EditAndCreate_Load(object sender, EventArgs e)
        {

        }

        private void button_cancel_Click(object sender, EventArgs e)
        {
            try
            {
                this.Close();
            }
            catch(Exception ex)
            {
                MessageBox.Show("Błąd podczas przełaczenia formów! " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void Knt_EditAndCreate_FormClosed(object sender, FormClosedEventArgs e)
        {
            formknt.Visible = true;
            formknt.Left = 0;
            formknt.Top = 2;
            formknt.Size = new Size(formknt.formMain.Size.Width - 20, formknt.formMain.Size.Height - formknt.formMain.sizeMenu - 70);
            formknt.formMain.formResize = formknt;
            formknt.formMain.toolStrip_menu.Enabled = true;
        }

        private void button_ok_Click(object sender, EventArgs e)
        {
            if (textBox_akrnoim.Text != "" && textBox_nazwa1.Text != "")
            {
                if (trybEdit)
                {
                
                    try
                    {
                        knt = new Knt_AllDane()
                        {
                            ID = knt.ID,
                            Akronim = textBox_akrnoim.Text,
                            Nazwa1 = textBox_nazwa1.Text,
                            Nazwa2 = textBox_nazwa2.Text,
                            Nazwa3 = textBox_nazwa3.Text,
                            NIP = textBox_nip.Text,
                            Miejscowosc = textBox_miejscowosc.Text,
                            KodPocztowy = textBox_kod.Text,
                            Ulica = textBox_ulica.Text,
                            NumerDomu = textBox_numerDomu.Text,
                            NumerLokum = textBox_numerLokalu.Text,
                            Kontakt = textBox_kontakt.Text,
                            CzyArchiwalny = (int)checkBox1.CheckState
                        };
                        UpdateSQL(knt);
                        nknt = new Knt()
                        {
                            ID = knt.ID,
                            Akronim = knt.Akronim,
                            Miejscowosc = knt.Miejscowosc,
                            Nazwa = knt.Nazwa1,
                            NIP = knt.NIP
                        };
                        formknt.updateListAndView(nknt, index, knt.CzyArchiwalny);
                        this.Close();
                    }
                    catch(Exception ex)
                    {
                        MessageBox.Show("Błąd podczas update! " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    try
                    {
                        knt = new Knt_AllDane()
                        {
                            Akronim = textBox_akrnoim.Text,
                            Nazwa1 = textBox_nazwa1.Text,
                            Nazwa2 = textBox_nazwa2.Text,
                            Nazwa3 = textBox_nazwa3.Text,
                            NIP = textBox_nip.Text,
                            Miejscowosc = textBox_miejscowosc.Text,
                            KodPocztowy = textBox_kod.Text,
                            Ulica = textBox_ulica.Text,
                            NumerDomu = textBox_numerDomu.Text,
                            NumerLokum = textBox_numerLokalu.Text,
                            Kontakt = textBox_kontakt.Text,
                            CzyArchiwalny = (int)checkBox1.CheckState
                        };
                        InsertSQL(knt);
                        nknt = FindOneKnt(knt.Akronim, knt.Nazwa1);
                        formknt.insertListnAndView(nknt, knt.CzyArchiwalny);
                        this.Close();
                    }
                    catch(Exception ex)
                    {
                        MessageBox.Show("Błąd podczas dodawania! " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                
            }
            else
            {
                MessageBox.Show("Nie uzupełniono wszystkich pól!", "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        public void UpdateSQL(Knt_AllDane knt)
        {

            IDbConnection db = cnn;
            if (db.State == ConnectionState.Closed)
                db.Open();
            string sqlQuery = "update dbo.kontrahenci set Nazwa1 = @Nazwa1,Nazwa2 =(case when @Nazwa2 <> '' then @Nazwa2 else null end),Nazwa3 = (case when @Nazwa3 <> '' then @Nazwa3 else null end),NIP = (case when cast(@NIP as bigint) <> '' then cast(@NIP as bigint) else null end),Akronim = @Akronim,KodPocztowy = (case when @KodPocztowy <> '' then @KodPocztowy else null end),Miejscowosc= (case when @Miejscowosc <> '' then @Miejscowosc else null end),Ulica = (case when @Ulica <> '' then @Ulica else null end),NumerDomu = (case when @NumerDomu <> '' then @NumerDomu else null end),NumerLokum = (case when @NumerLokum <> '' then @NumerLokum else null end), Kontakt=(case when @Kontakt <> '' then @Kontakt else null end), CzyArchiwalny = @CzyArchiwalny where id = @ID";
            db.Execute(sqlQuery, knt);
        }

        public void InsertSQL(Knt_AllDane knt)
        {
            IDbConnection db = cnn;
            if (db.State == ConnectionState.Closed)
                db.Open();
            string sqlQuery = "insert into dbo.kontrahenci select Nazwa1 = @Nazwa1,Nazwa2 =(case when @Nazwa2 <> '' then @Nazwa2 else null end),Nazwa3 = (case when @Nazwa3 <> '' then @Nazwa3 else null end),NIP = (case when cast(@NIP as bigint) <> '' then cast(@NIP as bigint) else null end),Akronim = @Akronim,KodPocztowy = (case when @KodPocztowy <> '' then @KodPocztowy else null end),Miejscowosc= (case when @Miejscowosc <> '' then @Miejscowosc else null end),Ulica = (case when @Ulica <> '' then @Ulica else null end),NumerDomu = (case when @NumerDomu <> '' then @NumerDomu else null end),NumerLokum = (case when @NumerLokum <> '' then @NumerLokum else null end), Kontakt=(case when @Kontakt <> '' then @Kontakt else null end), CzyArchiwalny = @CzyArchiwalny";
            db.Execute(sqlQuery, knt);
        }

        private void textBox_nip_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
                SystemSounds.Hand.Play();
            }
            else
            {

            }
        }

        public Knt FindOneKnt(string akronim, string nazwa)
        {
            try
            {
                IDbConnection db = cnn;
                if (db.State == ConnectionState.Closed)
                    db.Open();
                string sql = "select top(1) id,Akronim, Nazwa1 as Nazwa,NIP,Miejscowosc from dbo.kontrahenci where Akronim = @akronim and NAzwa1 = @nazwa order by id desc";
                return db.Query<Knt>(sql, new { akronim,nazwa }).SingleOrDefault();
            }
            catch (Exception ex)
            {
                throw new Exception("Błąd podczas selectcie po insercie! " + ex.Message);
            }
        }
    }
}
