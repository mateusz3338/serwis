﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Serwis
{
    public partial class FormDBConnection : Form
    {
        private FormLogin formLogin;
        public DataTable dataT;


        public FormDBConnection(FormLogin formLogin)
        {
            this.formLogin = formLogin;
            InitializeComponent();
            Dane();

        }


        private void DBConnection_Load(object sender, EventArgs e)
        {

        }


        private void FormDBConnection_FormClosed(object sender, FormClosedEventArgs e)
        {
            formLogin.AddConnectionString();
            formLogin.StartPosition = FormStartPosition.Manual;
            formLogin.Left = this.Left;
            formLogin.Top = this.Top;
            formLogin.Size = new Size(this.Width, this.Height);
            formLogin.Show();

        }

        private void buttonAdd_Click(object sender, EventArgs e)
        {
            FormDB_Create formDbC = new FormDB_Create(this);
            formDbC.StartPosition = FormStartPosition.Manual;
            formDbC.Left = this.Left;
            formDbC.Top = this.Top;
            formDbC.Size = new Size(this.Width, this.Height);
            this.Hide();
            formDbC.Show();
        }

        private void dataT_Ustawienia()
        {
            dataT = new DataTable();
            dataT.Columns.Add("Nazwa Połączenia");
            dataT.Columns.Add("Serwer");
            dataT.Columns.Add("Baza");
            dataT.Columns.Add("Login");
            dataT.Columns.Add("Hasło");
        }

        private void dataGridview_Ustawienia()
        {
            dataGridView1.Columns["Nazwa Połączenia"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            dataGridView1.Columns["Serwer"].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            dataGridView1.Columns["Baza"].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            dataGridView1.Columns["Login"].Visible = false;
            dataGridView1.Columns["Hasło"].Visible = false;
            
        }

        private void Dane()
        {
            try
            {
                dataT_Ustawienia();
                dataGridView1.DataSource = dataT;
                dataGridview_Ustawienia();
                LoadDane();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Błąd podczas ustawiania dataGridView: " + ex.Message,"Error",MessageBoxButtons.OK,MessageBoxIcon.Error);
            }
        }

        public void LoadDane()
        {
            dataT.Clear();
            Json js = new Json();
            List<connectionString> css = js.ReadListCnnStr();
            foreach (connectionString cs in css)
            {
                DataRow row = dataT.NewRow();
                row["Nazwa Połączenia"] = cs.Nazwa;
                row["Serwer"] = cs.Serwer;
                row["Baza"] = cs.Baza;
                row["Login"] = cs.LoginSzyfr;
                row["Hasło"] = cs.PassSzyfr;
                dataT.Rows.Add(row);
            }
        }

        private void buttonDel_Click(object sender, EventArgs e)
        {
            try
            {
                foreach (DataGridViewCell cell in dataGridView1.SelectedCells)
                {
                    try
                    {
                        DataRow row = dataT.Rows[cell.RowIndex];
                        Json js = new Json();
                        js.DellCnnStr(row["Nazwa Połączenia"].ToString());
                        dataGridView1.Rows.RemoveAt(cell.RowIndex);

                    }
                    catch {}

                }

            }
            catch(Exception ex)
            {
                MessageBox.Show("Błąd podczas usuwania: " + ex.Message);
            }
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void buttonCreate_Click(object sender, EventArgs e)
        {
            FormDB_Create formDbC = new FormDB_Create(this,true);
            formDbC.StartPosition = FormStartPosition.Manual;
            formDbC.Left = this.Left;
            formDbC.Top = this.Top;
            formDbC.Size = new Size(this.Width, this.Height);
            this.Hide();
            formDbC.Show();
        }
    }
}
