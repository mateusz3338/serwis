﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Serwis
{
    public partial class FormLogin : Form
    {
        private List<connectionString> css = new List<connectionString>();
        public FormLogin()
        {
            InitializeComponent();
            comboBox_db.Text = Properties.Settings.Default.cnnDB;
            AddConnectionString();

        }

        private void button1_Click(object sender, EventArgs e)
        {
            FormDBConnection formDbC = new FormDBConnection(this);
            formDbC.StartPosition = FormStartPosition.Manual;
            formDbC.Left = this.Left;
            formDbC.Top = this.Top;
            formDbC.Size = new Size(this.Width, this.Height);
            this.Hide();
            formDbC.Show();
        }

        private void FormLogin_Load(object sender, EventArgs e)
        {

        }

        private void buttonLogin_Click(object sender, EventArgs e)
        {
            Zaloguj();
        }

        private void Zaloguj()
        {
            try
            {
                if (comboBox_db.Text != "" && textBox_Login.Text != "")
                {
                    string serwer = "", baza = "", login = "", haslo = "";
                    foreach (connectionString cs in css)
                    {
                        if (cs.Nazwa == comboBox_db.Text)
                        {
                            serwer = cs.Serwer;
                            baza = cs.Baza;
                            login = cs.LoginSzyfrToString();
                            haslo = cs.PassSzyfrToString();
                            break;
                        }
                    }
                    if (serwer != "" && baza != "" && login != "" && haslo != "")
                    {
                        string cnnstr = SQL.ConnectionString(serwer, login, haslo, baza, false);
                        SQL.SettingConnection(cnnstr);
                        SQL.cnn.Open();
                        SqlCommand cmd = new SqlCommand(Properties.Resources.sprawdzLogin, SQL.cnn);
                        cmd.Parameters.Add("@login", SqlDbType.VarChar).Value = szyfr.Szyfrowanie(textBox_Login.Text.ToLower(), szyfr.Klucz);
                        cmd.Parameters.Add("@pass", SqlDbType.VarChar).Value = szyfr.Szyfrowanie(textBox_pass.Text, szyfr.Klucz);
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable dt = new DataTable();
                        da.Fill(dt);
                        string imie = "", nazwisko = "";
                        int opeid = 0;
                        if (dt.Rows.Count > 0)
                        {
                            opeid = Convert.ToInt32(dt.Rows[0]["id"]);
                            imie = dt.Rows[0]["Imie"].ToString();
                            nazwisko = dt.Rows[0]["Nazwisko"].ToString();
                        }
                        if (opeid > 0)
                        {
                            FormMain form = new FormMain(this, opeid, imie, nazwisko,SQL.cnn);
                            form.Show();
                            this.Hide();
                        }
                        else
                        {
                            MessageBox.Show("Nieprawidłowe dane logowania!", "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        SQL.cnn.Close();
                    }
                    else
                    {
                        MessageBox.Show(String.Format("Źle skonstruowany connection string {0}!", comboBox_db.Text), "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
                else
                {
                    MessageBox.Show("Nie uzupełnione wszystkie pola!", "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Błąd podczas logowania " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void AddConnectionString()
        {
            string text = comboBox_db.Text;
            comboBox_db.Text = "";
            Json js = new Json();
            css = js.ReadListCnnStr();
            comboBox_db.Items.Clear();
            foreach (connectionString cs in css)
            {
                if (text == cs.Nazwa)
                    comboBox_db.Text = text;
                comboBox_db.Items.Add(cs.Nazwa);
            }
        }

        private void FormLogin_FormClosed(object sender, FormClosedEventArgs e)
        {
            try
            {
                Properties.Settings.Default.cnnDB = comboBox_db.Text;
                Properties.Settings.Default.Save();
            }
            catch { }
        }

        private void textBox_pass_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Enter)
            {
                Zaloguj();
            }
        }
    }
}
