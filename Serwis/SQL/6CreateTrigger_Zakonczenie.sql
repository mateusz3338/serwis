declare @sql nvarchar(max) =
'
CREATE TRIGGER dbo.ZakonczenieWszystkichElementow
   ON  dbo.Elements
   AFTER update
AS 
BEGIN
	SET NOCOUNT ON;

	if update (DataOdbioru)
	begin
		declare @id_doc int = (select top(1) ID_doc from inserted)
		if not exists (select * from dbo.Elements where ID_DOC = @id_doc and DataOdbioru is null)
		begin
			update dbo.Documents
			set Status = 4
			where id = @id_doc
		end
		else
		begin
			if exists (select * from dbo.Elements where ID_DOC = @id_doc and DataOdbioru is not null)
			begin
				update dbo.Documents
				set Status = 3
				where id = @id_doc
			end
		end
	end

END'
execute(@sql)

