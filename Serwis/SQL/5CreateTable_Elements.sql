
create table Elements
(
ID int identity(1,1) primary key,
ID_DOC int foreign key references Documents(Id) not null,
ID_SPR int foreign key references Sprzet(ID) not null,
ID_OPEWYK int foreign key references Operatorzy(id) NULL,
OpisUszkodzenia varchar(2000) not null,
OpisNaprawy varchar(2000) null,
Koszt decimal(15,2),
Wycena decimal(15,2),
DataPrzyjecia Datetime not null,
DataZakonczenia datetime null,
DataOdbioru datetime,
CzyNaprawiono smallint not null
)


--CzyNaprawiono
-- 0 - Nie 
-- 1 - Tak