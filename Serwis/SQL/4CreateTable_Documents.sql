create table Documents
(
Id int identity(1,1) primary key,
ID_KNT int foreign key references Kontrahenci(ID) not null,
ID_Ope int foreign key references Operatorzy(ID) not null,
DataZgloszenia datetime not null, 
Numer varchar(25) null,
Status int not null 
)

--Status: 
-- 1 - w buforze
-- 2 - zatwierdzone
-- 3 - w realizacji
-- 4 - zakończone
-- 5 - Anulowane