declare @sql nvarchar(max) =
'
CREATE TRIGGER dbo.numerator
   ON  dbo.documents
   AFTER insert
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @id int 
	declare @data datetime
	declare @numer int 
	declare @numerator varchar(25)
	
	select top(1) @id = id, @data = DataZgloszenia
	from inserted

	select @numer = COUNT(*) +1
	from dbo.documents
	where MONTH(DataZgloszenia) = MONTH(@data) and YEAR(DataZgloszenia) = YEAR(@data) and id <> @id

	select @numerator = cast(@numer as varchar(10))+''/''+cast(MONTH(@data) as varchar(2))+''/''+cast(year(@data) as varchar(4))
	
	update dbo.Documents
	set Numer =@numerator
	where id = @id
END

'
execute(@sql)

