declare @Query nvarchar(max) = 'alter database {SERVERNAME} set single_user with rollback immediate
DROP database {SERVERNAME}'
set @Query = replace(@Query, '{SERVERNAME}', @DB)
EXECUTE(@Query)
