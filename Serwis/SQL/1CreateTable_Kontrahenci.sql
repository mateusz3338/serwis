create table kontrahenci
(
ID int identity(1,1) primary key,
Nazwa1 varchar(50) not null,
Nazwa2 varchar(50),
Nazwa3 varchar(50),
NIP bigint ,
Akronim varchar(50) not null,
KodPocztowy varchar(6),
Miejscowosc varchar(50),
Ulica varchar(50),
NumerDomu varchar(10),
NumerLokum varchar(10),
Kontakt varchar(255),
CzyArchiwalny smallint not null
)




--CzyArchiwalny:
-- 0 - nie 
-- 1 - tak