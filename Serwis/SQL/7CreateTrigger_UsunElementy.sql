declare @sql nvarchar(max) =
'
CREATE TRIGGER dbo.UsunElementy
   ON dbo.Documents
   AFTER delete
AS 
BEGIN

	SET NOCOUNT ON;
	declare @id int = (select top(1) id from inserted)
	delete from dbo.Elements
	where ID_DOC = @id

END
'
execute(@sql)


