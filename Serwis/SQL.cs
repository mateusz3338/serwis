﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Serwis
{
    static class SQL
    {
        public static SqlConnection cnn;

        public static string ConnectionString(string serwer, string login, string pass,string baza, bool czySzyfr)
        {
            string cStr = "";
            try
            {
                if (baza == "")
                {
                    if (czySzyfr == false)
                        cStr = String.Format(@"Data Source={0};User ID={1};Password={2};MultipleActiveResultSets=True", serwer, login, pass);
                    else
                        cStr = String.Format(@"Data Source={0};User ID={1};Password={2};MultipleActiveResultSets=True", serwer, login, szyfr.SzyfrToString(pass));
                }
                else
                {
                    if (czySzyfr == false)
                        cStr = String.Format(@"Data Source={0};Initial Catalog={1};User ID={2};Password={3};MultipleActiveResultSets=True",serwer,baza,login,pass);
                    else
                        cStr = String.Format(@"Data Source={0};Initial Catalog={1};User ID={2};Password={3};MultipleActiveResultSets=True", serwer, baza, login, szyfr.SzyfrToString(pass));
                }
            }
            catch(Exception ex)
            {
                throw new Exception("Błąd podczas tworzenie connectionString: " + ex.Message);
            }
            return cStr;
        }

        public static void SettingConnection(string cnnString)
        {
            cnn = new SqlConnection(cnnString);
        }

        public static int SqlInt(SqlConnection sqlCon, string sqlCmd)
        {
            int sqlInt = 0;
            SqlCommand selectCmd = new SqlCommand();
            selectCmd.Connection = sqlCon;
            selectCmd.CommandText = sqlCmd;
            try
            {
                SqlDataReader Reader = selectCmd.ExecuteReader();
                if (Reader.HasRows)
                {
                    while (Reader.Read())
                    {
                        sqlInt = Reader.GetInt32(0);
                    }
                }
            }
            catch
            {

                throw new Exception(string.Format("Błąd wykonywania zapytania |{0}|", sqlCmd));
            }

            return sqlInt;
        }

    }
}
