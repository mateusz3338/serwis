﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Serwis
{
    static class szyfr
    {
        private static bool bladKlucza = false;

        public static string Klucz = "dc7caa69f44278ea42fdc2ca896ea42948ecb4930c63d35f4da30e399be40dd4;c524afdaeeb3d692d173cea9742cb7aa";

        public static string SzyfrToString(string tresc)
        {
            string StringReturn = "";
            try
            {
                StringReturn = szyfr.Deszyfruj(tresc, Klucz);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Błąd podczas zmiany Szyfru na String! " + ex.Message, "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return StringReturn;
        }

        private static byte[] ToHexBytes(this string hex)
        {
            if (hex == null) return null;
            if (hex.Length == 0) return new byte[0];

            int l = hex.Length / 2;
            var b = new byte[l];
            for (int i = 0; i < l; ++i)
            {
                b[i] = Convert.ToByte(hex.Substring(i * 2, 2), 16);
            }
            return b;
        }

        private static string ToHexString(this byte[] hex)
        {
            if (hex == null) return null;
            if (hex.Length == 0) return string.Empty;

            var s = new StringBuilder();
            foreach (byte b in hex)
            {
                s.Append(b.ToString("x2"));
            }
            return s.ToString();
        }

        public static string Szyfrowanie(string original, string klucz)
        {
            string text = "";
            try
            {

                Aes myAes = Aes.Create();

                using (myAes = KeyToAes(klucz))
                {
                    if (bladKlucza == false)
                    {
                        byte[] encrypted = EncryptStringToBytes_Aes(original, myAes.Key, myAes.IV);
                        text = ToHexString(encrypted);
                    }
                }

            }
            catch (Exception e)
            {
                MessageBox.Show(string.Format("Error: {0}", e.Message), "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return text;
        }

        public static string Deszyfruj(string original, string klucz)
        {
            string text = "";
            try
            {

                Aes myAes = Aes.Create();

                using (myAes = KeyToAes(klucz))
                {
                    if (bladKlucza == false)
                    {
                        byte[] wiadomosc = ToHexBytes(original);
                        string roundtrip = DecryptStringFromBytes_Aes(wiadomosc, myAes.Key, myAes.IV);
                        text = roundtrip;
                    }
                }

            }
            catch (Exception e)
            {
                MessageBox.Show(string.Format("Error: {0}", e.Message), "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return text;
        }

        static byte[] EncryptStringToBytes_Aes(string plainText, byte[] Key, byte[] IV)
        {

            if (plainText == null || plainText.Length <= 0)
                throw new ArgumentNullException("plainText");
            if (Key == null || Key.Length <= 0)
                throw new ArgumentNullException("Key");
            if (IV == null || IV.Length <= 0)
                throw new ArgumentNullException("IV");
            byte[] encrypted;

            using (Aes aesAlg = Aes.Create())
            {
                aesAlg.Key = Key;
                aesAlg.IV = IV;

                ICryptoTransform encryptor = aesAlg.CreateEncryptor(aesAlg.Key, aesAlg.IV);

                using (MemoryStream msEncrypt = new MemoryStream())
                {
                    using (CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                    {
                        using (StreamWriter swEncrypt = new StreamWriter(csEncrypt))
                        {

                            swEncrypt.Write(plainText);
                        }
                        encrypted = msEncrypt.ToArray();
                    }
                }
            }

            return encrypted;

        }

        private static string ByteToString(byte[] klucz)
        {
            string key = "";
            try
            {
                foreach (byte znak in klucz)
                {
                    key += znak.ToString() + ",";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Błąd podczas konwersji Byte to String. " + ex.Message, "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return key;
        }

        static string DecryptStringFromBytes_Aes(byte[] cipherText, byte[] Key, byte[] IV)
        {
            if (cipherText == null || cipherText.Length <= 0)
                throw new ArgumentNullException("cipherText");
            if (Key == null || Key.Length <= 0)
                throw new ArgumentNullException("Key");
            if (IV == null || IV.Length <= 0)
                throw new ArgumentNullException("IV");

            string plaintext = null;

            using (Aes aesAlg = Aes.Create())
            {
                aesAlg.Key = Key;
                aesAlg.IV = IV;


                ICryptoTransform decryptor = aesAlg.CreateDecryptor(aesAlg.Key, aesAlg.IV);

                using (MemoryStream msDecrypt = new MemoryStream(cipherText))
                {
                    using (CryptoStream csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                    {
                        using (StreamReader srDecrypt = new StreamReader(csDecrypt))
                        {
                            plaintext = srDecrypt.ReadToEnd();
                        }
                    }
                }

            }

            return plaintext;

        }

        private static byte[] StringToByte(string[] tresc)
        {
            byte[] bKeyReturn = new byte[0];
            int z = 0;
            try
            {
                byte[] bKey;
                foreach (var test in tresc)
                {
                    if (test.ToString() != "")
                    {
                        z++;
                    }
                }
                bKey = new byte[z];
                z = 0;
                foreach (var test in tresc)
                {
                    if (test.ToString() != "")
                    {
                        byte t = Convert.ToByte(test.ToString());
                        bKey[z] = t;
                        z++;
                    }
                }
                bKeyReturn = bKey;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Błąd podczas konwersji String to Byte. " + ex.Message, "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return bKeyReturn;
        }

        public static Aes KeyToAes(string Klucz)
        {
            Aes myAes = Aes.Create();
            string[] Podzial = Klucz.Split(';');
            int i = 0;
            foreach (var test in Podzial)
            {
                i++;
            }
            if (i == 2)
            {

                string Key = Podzial[0];
                string IV = Podzial[1];
                byte[] bKey = ToHexBytes(Key);
                byte[] bIV = ToHexBytes(IV);

                myAes.CreateEncryptor(bKey, bIV);
                myAes.CreateDecryptor(bKey, bIV);
                myAes.Key = bKey;
                myAes.IV = bIV;
                bladKlucza = false;
            }
            else
            {
                MessageBox.Show("Błędny Klucz!");
                bladKlucza = true;
            }
            return myAes;
        }

        public static string GenerujKlucz()
        {
            string Klucz = "";
            try
            {
                string key = KeyFromAES();
                if (key != "")
                {
                    Klucz = Convert.ToString(key);
                }
                else
                {
                    MessageBox.Show("Błąd podczas generowania Klucza. Klucz jest pusty!", "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Błąd podczas generowania klucza! " + ex.Message, "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return Klucz;
        }

        private static string KeyFromAES()
        {
            string key = "";
            try
            {
                Aes myAes = Aes.Create();

                key = ToHexString(myAes.Key);
                key += ";";
                key += ToHexString(myAes.IV);

            }
            catch (Exception ex)
            {
                MessageBox.Show("Błąd podczas Generowania klucza: " + ex.Message, "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return key;
        }
    }
}
