﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Serwis
{
    static class Exit
    {
        public static void CloseApp()
        {
            System.Windows.Forms.Application.Exit();
        }

        public static void CloseAppSQL(SqlConnection cnn)
        {
            if (cnn.State == System.Data.ConnectionState.Open)
                cnn.Close();
            System.Windows.Forms.Application.Exit();
        }
    }
}
