﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Serwis
{
    public class Knt
    {
        public int ID { get; set; }
        public string Akronim { get; set; }
        public string Nazwa { get; set; }
        public string NIP { get; set; }
        public string Miejscowosc { get; set; }
    }

    public class Knt_AllDane
    {
        public int ID { get; set; }
        public string Nazwa1 { get; set; } //50
        public string Nazwa2 { get; set; } //50
        public string Nazwa3 { get; set; } //50
        public string NIP { get; set; } //long
        public string Akronim { get; set; } //50
        public string KodPocztowy { get; set; } //6
        public string Miejscowosc { get; set; } //50
        public string Ulica { get; set; } //50
        public string NumerDomu { get; set; }//10
        public string NumerLokum { get; set; }//10
        public string Kontakt { get; set; }//255
        public int CzyArchiwalny { get; set; }//Smallint
    }

    public class Sprzet
    {
        public int ID { get; set; }
        public string Nazwa { get; set; }
        public string Model { get; set; }
    }

    public class SprzetList
    {
        public int ID { get; set; }
        public string FullNazwa { get; set; }
    }

    public class Operatorzy
    {
        public int ID { get; set; }
        public string Imie { get; set; }
        public string Nazwisko { get; set; }
        public DateTime DataZatrudnienia { get; set; }
        public DateTime DataZwolnienia { get; set; }
        public string Login { get; set; }
        public string Haslo { get; set; }
    }

    public class Dokument
    {
        public int ID { get; set; }
        public int ID_KNT { get; set; }
        public int ID_OPE { get; set; }
        public string Kontrahent { get; set; } //50
        public string Operator { get; set; } //101
        public DateTime DataZgloszenia { get; set; }
        public string Numer { get; set; }//25
        public string Status { get; set; }
    }

    public class KntAkronim
    {
        public int ID { get; set; }
        public string Akronim { get; set; }
    }

    public class Elements
    {
        public int Lp { get; set; }
        public int ID { get; set; }
        public int ID_DOC { get; set; }
        public int ID_SPR { get; set; }
        public int ID_OPEWYK { get; set; }
        public string OperatorW { get; set; }
        public string Sprzet { get; set; }
        public string OpisUszkodzenia { get; set; } //2000
        public string OpisNaprawy { get; set; } //2000
        public decimal Koszt { get; set; } //15,2
        public decimal Wycena { get; set; } //15,2
        public DateTime DataPrzyjecia { get; set; }
        public DateTime DataZakonczenia { get; set; }
        public DateTime DataOdbioru { get; set; }
        public int CzyNaprawiono { get; set; } //smallint

    }
}
